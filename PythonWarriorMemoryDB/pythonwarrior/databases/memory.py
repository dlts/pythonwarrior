# -*- coding: utf-8 -*-

from pythonwarrior.databases.interface import MapperFactory, RecordMapper


class MemoryMapperFactory(MapperFactory):

    def __init__(self):
        self.mappers = dict()

    def create_mapper(self, record_type):
        if record_type not in self.mappers:
            self.mappers[record_type] = MemoryRecordMapper(record_type._fields[0])
        return self.mappers[record_type]


class MemoryRecordMapper(RecordMapper):

    def __init__(self, key_name):
        self.key_name = key_name
        self.records = dict()

    def read(self, key):
        try:
            return self.records[key]
        except KeyError:
            raise RecordMapper.NoSuchRecord()

    def read_all(self):
        return [record for record in self.records.values()]

    def create(self, record):
        key = self.get_key(record)
        if key in self.records:
            raise RecordMapper.DuplicateRecord()
        self.records[key] = record

    def update(self, record):
        key = self.get_key(record)
        self.assert_record_exists(key)
        self.records[key] = record

    def assert_record_exists(self, key):
        if key not in self.records:
            raise RecordMapper.NoSuchRecord()

    def delete(self, key):
        self.assert_record_exists(key)
        del self.records[key]

    def get_key(self, record):
        return getattr(record, self.key_name)

