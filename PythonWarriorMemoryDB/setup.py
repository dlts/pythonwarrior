from setuptools import setup

setup(
    name='PythonWarrior Memory DB',
    version='',
    packages=["pythonwarrior.databases"],
    namespaces=["pythonwarrior.databases"],
    namespace_packages=["pythonwarrior.databases"],
    url='',
    license='',
    author='wkmanire',
    author_email='williamkmanire@gmail.com',
    description='',
)
