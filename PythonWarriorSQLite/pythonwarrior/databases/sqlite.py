# -*- coding: utf-8 -*-
__author__ = 'agates'

import itertools
import sqlite3

from pythonwarrior.databases.interface import *


class SQLiteMapperFactory(MapperFactory):
    def __init__(self, database=':memory:'):
        self.mappers = dict()
        self.conn = sqlite3.connect(database=database)

        # Enable foreign keys for the current connection
        self.conn.cursor().execute('PRAGMA foreign_keys = ON;')
        self.conn.commit()

    def create_mapper(self, record_type):
        if record_type not in self.mappers:
            self.mappers[record_type] = record_mappers[record_type](self.conn)
        return self.mappers[record_type]


class SQLiteRecordMapper(RecordMapper):
    def __init__(self, conn):
        self.conn = conn

    def commit(self):
        self.conn.commit()

    def cursor(self):
        return self.conn.cursor()

    def rollback(self):
        self.conn.rollback()

    @staticmethod
    def assert_record_exists(record):
        if record is None:
            raise RecordMapper.NoSuchRecord()

    @abstractmethod
    def create(self, record):
        pass

    @abstractmethod
    def read(self, key):
        pass

    @abstractmethod
    def read_all(self):
        pass

    @abstractmethod
    def update(self, record):
        pass

    @abstractmethod
    def delete(self, primary_key):
        pass


class AIEffectRecordMapper(SQLiteRecordMapper):
    def read(self, name):
        c = self.cursor()
        try:
            c.execute('SELECT name, code FROM ai_effect WHERE name=?;', (name,))
            record = c.fetchone()
            self.assert_record_exists(record)
            return EffectRecord._make(record)
        except sqlite3.Error as e:
            raise e

    def read_all(self):
        c = self.cursor()
        try:
            c.execute('SELECT name, code FROM ai_effect;')
            # This could be an empty list
            return frozenset(map(EffectRecord._make, c.fetchall()))
        except sqlite3.Error as e:
            raise e

    def create(self, record):
        c = self.cursor()
        try:
            c.execute('INSERT INTO ai_effect VALUES (?, ?);', record)
            self.commit()
        except sqlite3.IntegrityError as e:
            self.rollback()
            raise RecordMapper.DuplicateRecord()
        except sqlite3.Error as e:
            self.rollback()
            raise e

    def update(self, record):
        c = self.cursor()
        try:
            c.execute('UPDATE ai_effect SET code=:code WHERE name=:name;', record._asdict())
            self.commit()
        except sqlite3.Error as e:
            self.rollback()
            raise e
            #raise RecordMapper.NoSuchRecord()

    def delete(self, name):
        c = self.cursor()
        try:
            c.execute('DELETE FROM ai_effect WHERE name=?;', (name,))
            self.commit()
        except sqlite3.Error as e:
            self.rollback()
            raise e
            #raise RecordMapper.NoSuchRecord()


class EffectRecordMapper(SQLiteRecordMapper):
    def read(self, name):
        c = self.cursor()
        try:
            c.execute('SELECT name, code FROM effect WHERE name=?;', (name,))
            record = c.fetchone()
            self.assert_record_exists(record)
            return EffectRecord._make(record)
        except sqlite3.Error as e:
            raise e

    def read_all(self):
        c = self.cursor()
        try:
            c.execute('SELECT name, code FROM effect;')
            # This could be an empty list
            return frozenset(map(EffectRecord._make, c.fetchall()))
        except sqlite3.Error as e:
            raise e

    def create(self, record):
        c = self.cursor()
        try:
            c.execute('INSERT INTO effect VALUES (?, ?);', record)
            self.commit()
        except sqlite3.IntegrityError as e:
            self.rollback()
            raise RecordMapper.DuplicateRecord()
        except sqlite3.Error as e:
            self.rollback()
            raise e

    def update(self, record):
        c = self.cursor()
        try:
            c.execute('UPDATE effect SET code=:code WHERE name=:name;', record._asdict())
            self.commit()
        except sqlite3.Error as e:
            self.rollback()
            raise e
            #raise RecordMapper.NoSuchRecord()

    def delete(self, name):
        c = self.cursor()
        try:
            c.execute('DELETE FROM effect WHERE name=?;', (name,))
            self.commit()
        except sqlite3.Error as e:
            self.rollback()
            raise e
            #raise RecordMapper.NoSuchRecord()


class ActionRecordMapper(SQLiteRecordMapper):
    def get_action_effects_by_type(self, action_name, effect_type):
        c = self.cursor()
        try:
            c.execute('SELECT {0}_name FROM action_{0} '
                      'WHERE action_name=?;'.format(effect_type), (action_name,))
            records = c.fetchall()
            self.assert_record_exists(records)
            return frozenset(r[0] for r in records)
        except sqlite3.Error as e:
            raise e

    def get_actor_effects(self, action_name):
        return self.get_action_effects_by_type(action_name, 'actor_effect')

    def get_target_effects(self, action_name):
        return self.get_action_effects_by_type(action_name, 'target_effect')

    def read(self, name):
        c = self.cursor()
        try:
            c.execute('SELECT label FROM action WHERE name=?;', (name,))
            record = c.fetchone()
            self.assert_record_exists(record)
            label = record[0]
            actor_effect_names = self.get_actor_effects(name)
            target_effect_names = self.get_target_effects(name)

            return ActionRecord(name, label, actor_effect_names, target_effect_names)
        except sqlite3.Error as e:
            raise e

    def read_all(self):
        c = self.cursor()
        try:
            def make_action_record(r):
                return ActionRecord(r[0], r[1],
                                    self.get_actor_effects(r[0]),
                                    self.get_target_effects(r[0]))

            c.execute('SELECT name, label FROM action;')

            # This could be an empty list
            return frozenset(map(make_action_record, c.fetchall()))
        except sqlite3.Error as e:
            raise e

    def create(self, record):
        c = self.cursor()
        try:
            record = record._asdict()
            c.execute('INSERT INTO action VALUES (:name, :label);', record)

            c.executemany('INSERT INTO action_actor_effect(action_name, actor_effect_name) '
                          'VALUES(?, ?);', ((record['name'], r) for r in record['actor_effect_names']))
            c.executemany('INSERT INTO action_target_effect(action_name, target_effect_name) '
                          'VALUES(?, ?);', ((record['name'], r) for r in record['target_effect_names']))

            self.commit()
        except sqlite3.IntegrityError as e:
            self.rollback()
            raise RecordMapper.DuplicateRecord()
        except sqlite3.Error as e:
            self.rollback()
            raise e

    def update(self, record):
        c = self.cursor()
        try:
            # Create a temporary table to store values in
            c.execute('CREATE TEMPORARY TABLE temp_effect(effect_name TEXT PRIMARY KEY) WITHOUT ROWID;')

            record = record._asdict()
            # Update the main record
            c.execute('UPDATE action SET label=:label WHERE name=:name;', record)

            # Insert actor effects into temp table for usage below
            c.executemany('INSERT INTO temp_effect(effect_name) '
                          'VALUES(?);', ((r,) for r in record['actor_effect_names']))
            # Insert or replace the values from the temp table
            c.execute('INSERT OR REPLACE INTO action_actor_effect(action_name, actor_effect_name) '
                      'SELECT ? as action_name, effect_name FROM temp_effect;', (record['name'],))
            # Delete all rows which don't have values inside the temp table
            c.execute('DELETE FROM action_actor_effect '
                      'WHERE actor_effect_name NOT IN (SELECT effect_name FROM temp_effect);')
            c.execute('DELETE FROM temp_effect;')

            # Same procedure as above but for target effects
            c.executemany('INSERT INTO temp_effect(effect_name) '
                          'VALUES(?);', ((r,) for r in record['target_effect_names']))
            c.execute('INSERT OR REPLACE INTO action_target_effect(action_name, target_effect_name) '
                      'SELECT ? as action_name, effect_name FROM temp_effect;', (record['name'],))
            c.execute('DELETE FROM action_target_effect '
                      'WHERE target_effect_name NOT IN (SELECT effect_name FROM temp_effect);')

            c.execute('DROP TABLE temp_effect')
            self.commit()
        except sqlite3.IntegrityError as e:
            self.rollback()
            raise RecordMapper.DuplicateRecord()
        except sqlite3.Error as e:
            self.rollback()
            raise e

    def delete(self, name):
        c = self.cursor()
        try:
            c.execute('DELETE FROM action WHERE name=?;', (name,))
            self.commit()
        except sqlite3.Error as e:
            self.rollback()
            raise e
            #raise RecordMapper.NoSuchRecord()


class EquipmentTypeRecordMapper(SQLiteRecordMapper):
    def read(self, name):
        c = self.cursor()
        try:
            # This is a useless statement for now, but we might expand it later
            c.execute('SELECT name FROM equipment_type WHERE name=?;', (name,))
            record = c.fetchone()
            self.assert_record_exists(record)
            return EquipmentTypeRecord._make(record)
        except sqlite3.Error as e:
            raise e

    def read_all(self):
        c = self.cursor()
        try:
            c.execute('SELECT name FROM equipment_type;')
            # This could be an empty list
            return frozenset(map(EquipmentTypeRecord._make, c.fetchall()))
        except sqlite3.Error as e:
            raise e

    def create(self, record):
        c = self.cursor()
        try:
            c.execute('INSERT INTO equipment_type VALUES (?);', record)
            self.commit()
        except sqlite3.IntegrityError as e:
            self.rollback()
            raise RecordMapper.DuplicateRecord()
        except sqlite3.Error as e:
            self.rollback()
            raise e

    def update(self, record):
        c = self.cursor()
        try:
            # useless, only item we have is the primary key
            #c.execute('UPDATE equipment_type ----- WHERE name=:name;', record._asdict())
            self.commit()
        except sqlite3.Error as e:
            self.rollback()
            raise e
            #raise RecordMapper.NoSuchRecord()

    def delete(self, name):
        c = self.cursor()
        try:
            c.execute('DELETE FROM equipment_type WHERE name=?;', (name,))
            self.commit()
        except sqlite3.Error as e:
            self.rollback()
            raise e
            #raise RecordMapper.NoSuchRecord()


class EquipmentRecordMapper(SQLiteRecordMapper):
    def get_equipment_step_effects_by_name(self, equipment_name):
        c = self.cursor()
        try:
            c.execute('SELECT step_effect_name FROM equipment_step_effect '
                      'WHERE equipment_name=?;', (equipment_name,))
            records = c.fetchall()
            self.assert_record_exists(records)
            return frozenset(r[0] for r in records)
        except sqlite3.Error as e:
            raise e

    def read(self, name):
        c = self.cursor()
        try:
            c.execute('SELECT equipment_type_name, power FROM equipment WHERE name=?;', (name,))
            record = c.fetchone()
            self.assert_record_exists(record)
            equipment_type_name = record[0]
            power = record[1]
            equipment_step_effect_names = self.get_equipment_step_effects_by_name(name)

            return EquipmentRecord(name, equipment_type_name, power, equipment_step_effect_names)
        except sqlite3.Error as e:
            raise e

    def read_all(self):
        c = self.cursor()
        try:
            def make_equipment_record(r):
                return EquipmentRecord(r[0], r[1], r[2], self.get_equipment_step_effects_by_name(r[0]))

            c.execute('SELECT name, equipment_type_name, power FROM equipment;')

            # This could be an empty list
            return frozenset(map(make_equipment_record, c.fetchall()))
        except sqlite3.Error as e:
            raise e

    def create(self, record):
        c = self.cursor()
        try:
            record = record._asdict()

            c.execute('INSERT INTO equipment VALUES (:name, :equipment_type_name, :power);', record)

            c.executemany('INSERT INTO equipment_step_effect(equipment_name, step_effect_name) '
                          'VALUES(?, ?);', ((record['name'], r) for r in record['step_effect_names']))
            self.commit()
        except sqlite3.IntegrityError as e:
            self.rollback()
            raise RecordMapper.DuplicateRecord()
        except sqlite3.Error as e:
            self.rollback()
            raise e

    def update(self, record):
        c = self.cursor()
        try:
            # Create a temporary table to store values in
            c.execute('CREATE TEMPORARY TABLE temp_effect(effect_name TEXT PRIMARY KEY) WITHOUT ROWID;')

            record = record._asdict()

            # Update the main record
            c.execute('UPDATE equipment SET equipment_type_name=:equipment_type_name, power=:power WHERE name=:name;', record)

            # Insert step effects into temp table for usage below
            c.executemany('INSERT INTO temp_effect(effect_name) '
                          'VALUES(?);', ((r,) for r in record['step_effect_names']))
            # Insert or replace the values from the temp table
            c.execute('INSERT OR REPLACE INTO equipment_step_effect(equipment_name, step_effect_name) '
                      'SELECT ? as equipment_name, effect_name FROM temp_effect;', (record['name'],))
            # Delete all rows which don't have values inside the temp table
            c.execute('DELETE FROM equipment_step_effect '
                      'WHERE step_effect_name NOT IN (SELECT effect_name FROM temp_effect);')

            c.execute('DROP TABLE temp_effect')
            self.commit()
        except sqlite3.IntegrityError as e:
            self.rollback()
            raise RecordMapper.DuplicateRecord()
        except sqlite3.Error as e:
            self.rollback()
            raise e

    def delete(self, name):
        c = self.cursor()
        try:
            c.execute('DELETE FROM equipment WHERE name=?;', (name,))
            self.commit()
        except sqlite3.Error as e:
            self.rollback()
            raise e
            #raise RecordMapper.NoSuchRecord()


class LevelRecordMapper(SQLiteRecordMapper):
    def read(self, number):
        c = self.cursor()
        try:
            c.execute('SELECT experience, action_name, base_strength, base_agility, '
                      'base_hp, base_mp FROM level WHERE number=?;', (number,))
            record = c.fetchone()
            self.assert_record_exists(record)

            return LevelRecord(number, record[0], record[1], record[2], record[3], record[4], record[5])
        except sqlite3.Error as e:
            raise e

    def read_all(self):
        c = self.cursor()
        try:
            def make_level_record(r):
                return LevelRecord(r[0], r[1], r[2], r[3], r[4], r[5], r[6])

            c.execute('SELECT number, experience, action_name, base_strength, base_agility, '
                      'base_hp, base_mp FROM level;')

            # This could be an empty list
            return frozenset(map(make_level_record, c.fetchall()))
        except sqlite3.Error as e:
            raise e

    def create(self, record):
        c = self.cursor()
        try:
            record = record._asdict()
            c.execute('INSERT INTO level VALUES (:number, :experience, :action_name, :base_strength, '
                      ':base_agility, :base_hp, :base_mp);', record)

            self.commit()
        except sqlite3.IntegrityError as e:
            self.rollback()
            raise RecordMapper.DuplicateRecord()
        except sqlite3.Error as e:
            self.rollback()
            raise e

    def update(self, record):
        c = self.cursor()
        try:
            record = record._asdict()

            c.execute('UPDATE level SET experience=:experience, action_name=:action_name, '
                      'base_strength=:base_strength, base_agility=:base_agility, base_hp=:base_hp, '
                      'base_mp=:base_mp WHERE number=:number;', record)
            self.commit()
        except sqlite3.IntegrityError as e:
            self.rollback()
            raise RecordMapper.DuplicateRecord()
        except sqlite3.Error as e:
            self.rollback()
            raise e

    def delete(self, number):
        c = self.cursor()
        try:
            c.execute('DELETE FROM level WHERE number=?;', (number,))
            self.commit()
        except sqlite3.Error as e:
            self.rollback()
            raise e
            #raise RecordMapper.NoSuchRecord()


record_mappers = {
    ActionRecord: ActionRecordMapper,
    AIEffectRecord: AIEffectRecordMapper,
    EffectRecord: EffectRecordMapper,
    EquipmentRecord: EquipmentRecordMapper,
    EquipmentTypeRecord: EquipmentTypeRecordMapper,
    LevelRecord: LevelRecordMapper
}