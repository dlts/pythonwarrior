from setuptools import setup

setup(
    name='PythonWarrior SQLite DB',
    version='',
    packages=["pythonwarrior.databases"],
    namespaces=["pythonwarrior.databases"],
    namespace_packages=["pythonwarrior.databases"],
    url='',
    license='',
    author='agates',
    author_email='alecks.g@gmail.com',
    description='',
)
