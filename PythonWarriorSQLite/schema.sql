PRAGMA foreign_keys = ON;

-- Have to remove tables with dependencies on others first
DROP TABLE IF EXISTS action_actor_effect;
DROP TABLE IF EXISTS action_target_effect;
DROP TABLE IF EXISTS level;
DROP TABLE IF EXISTS monster_action;
DROP TABLE IF EXISTS action;
DROP TABLE IF EXISTS effect;
DROP TABLE IF EXISTS equipment;
DROP TABLE IF EXISTS equipment_type;
DROP TABLE IF EXISTS equipment_step_effect;
DROP TABLE IF EXISTS hero;
DROP TABLE IF EXISTS inventory;
DROP TABLE IF EXISTS item;
DROP TABLE IF EXISTS ai_effect;
DROP TABLE IF EXISTS monster;

CREATE TABLE effect(
	name TEXT PRIMARY KEY,
	code TEXT UNIQUE NOT NULL
) WITHOUT ROWID;

CREATE TABLE action(
	name TEXT PRIMARY KEY,
	label TEXT UNIQUE NOT NULL
) WITHOUT ROWID;

CREATE TABLE action_actor_effect(
	action_name TEXT,
	actor_effect_name TEXT,
	PRIMARY KEY (action_name, actor_effect_name),
	FOREIGN KEY (action_name) REFERENCES action(name) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (actor_effect_name) REFERENCES effect(name) ON UPDATE CASCADE ON DELETE RESTRICT
) WITHOUT ROWID;

CREATE TABLE action_target_effect(
	action_name TEXT,
	target_effect_name TEXT,
	PRIMARY KEY (action_name, target_effect_name),
	FOREIGN KEY (action_name) REFERENCES action(name) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (target_effect_name) REFERENCES effect(name) ON UPDATE CASCADE ON DELETE RESTRICT
) WITHOUT ROWID;

CREATE TABLE level(
	number INT PRIMARY KEY,
	experience INT NOT NULL CHECK(experience >= 0),
	action_name TEXT,
	base_strength INT NOT NULL CHECK(base_strength > 0),
	base_agility INT NOT NULL CHECK(base_agility > 0),
	base_hp INT NOT NULL CHECK(base_hp > 0),
	base_mp INT NOT NULL CHECK(base_mp > 0),
	FOREIGN KEY (action_name) REFERENCES action(name) ON UPDATE CASCADE ON DELETE RESTRICT
) WITHOUT ROWID;

CREATE TABLE equipment_type(
	name TEXT PRIMARY KEY
) WITHOUT ROWID;

CREATE TABLE equipment(
	name TEXT PRIMARY KEY,
	equipment_type_name TEXT NOT NULL,
	power INT NOT NULL CHECK(power > 0),
	FOREIGN KEY (equipment_type_name)  REFERENCES equipment_type(name) ON UPDATE CASCADE ON DELETE RESTRICT
) WITHOUT ROWID;

CREATE TABLE equipment_step_effect(
	equipment_name TEXT,
	step_effect_name TEXT,
	PRIMARY KEY (equipment_name, step_effect_name),
	FOREIGN KEY (equipment_name) REFERENCES equipment(name) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (step_effect_name) REFERENCES effect(name) ON UPDATE CASCADE ON DELETE RESTRICT
) WITHOUT ROWID;

CREATE TABLE hero(
	name TEXT PRIMARY KEY,
	experience INT NOT NULL,
	hp INT NOT NULL,
	mp INT NOT NULL,
	ring_name TEXT,
	weapon_name TEXT,
	armor_name TEXT,
	shield_name TEXT,
	accessory_name TEXT,
	FOREIGN KEY (ring_name) REFERENCES equipment(name) ON UPDATE CASCADE ON DELETE RESTRICT,
	FOREIGN KEY (weapon_name) REFERENCES equipment(name) ON UPDATE CASCADE ON DELETE RESTRICT,
	FOREIGN KEY (armor_name) REFERENCES equipment(name) ON UPDATE CASCADE ON DELETE RESTRICT,
	FOREIGN KEY (shield_name) REFERENCES equipment(name) ON UPDATE CASCADE ON DELETE RESTRICT,
	FOREIGN KEY (accessory_name) REFERENCES equipment(name) ON UPDATE CASCADE ON DELETE RESTRICT
) WITHOUT ROWID;

CREATE TABLE item(
	name TEXT PRIMARY KEY,
	is_stackable INT NOT NULL CHECK(is_stackable=0 OR is_stackable=1),
	effect_name TEXT,
	FOREIGN KEY (effect_name) REFERENCES effect(name) ON UPDATE  CASCADE ON DELETE RESTRICT
) WITHOUT ROWID;

CREATE TABLE inventory(
	hero_name TEXT,
	item_name TEXT,
	quantity INT NOT NULL CHECK(quantity > 0),
	PRIMARY KEY (hero_name, item_name),
	FOREIGN KEY (hero_name) REFERENCES hero(name) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY(item_name) REFERENCES item(name) ON UPDATE CASCADE ON DELETE RESTRICT
) WITHOUT ROWID;

CREATE TABLE ai_effect(
	name TEXT PRIMARY KEY,
	code TEXT UNIQUE NOT NULL
) WITHOUT ROWID;

CREATE TABLE monster(
	name TEXT PRIMARY KEY,
	strength INT NOT NULL CHECK(strength > 0),
	agility INT NOT NULL CHECK(agility > 0),
	max_hp INT NOT NULL CHECK(max_hp > 0),
	max_mp INT NOT NULL CHECK(max_mp > 0),
	gold INT NOT NULL CHECK(gold >= 0),
	experience INT NOT NULL CHECK(experience >= 0),
	ai_effect_name TEXT,
	FOREIGN KEY (ai_effect_name) REFERENCES ai_effect(name) ON UPDATE CASCADE ON DELETE RESTRICT
) WITHOUT ROWID;

CREATE TABLE monster_action(
	monster_name TEXT,
	action_name TEXT,
	PRIMARY KEY (monster_name, action_name),
	FOREIGN KEY (monster_name) REFERENCES monster(name) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (action_name) REFERENCES action(name) ON UPDATE CASCADE ON DELETE RESTRICT
) WITHOUT ROWID;