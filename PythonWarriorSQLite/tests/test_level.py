# -*- coding: utf-8 -*-
__author__ = 'agates'

import itertools
import random

from pythonwarrior.databases.interface import ActionRecord, EffectRecord, LevelRecord, RecordMapper

from test_base import BaseTestCase


class ActionRecordTestCase(BaseTestCase):
    def setUp(self):
        def effect_from_num(num):
            return EffectRecord('test_name{0}'.format(num), 'test_code{0}'.format(num))

        def action_from_num(num, actor_effects, target_effects):
            return ActionRecord('action_name{0}'.format(num), 'action_label{0}'.format(num),
                                frozenset(r[0] for r in actor_effects), frozenset(r[0] for r in target_effects))

        def level_from_num(num, actions):
            return LevelRecord(number=num,
                               experience=random.randint(0, 30000),
                               action_name=random.sample(actions, 1)[0]._asdict()['name'],
                               base_strength=random.randint(1, 30000),
                               base_agility=random.randint(10, 30000),
                               base_hp=random.randint(1, 30000),
                               base_mp=random.randint(1, 30000))

        self.baseSetUp()
        self.effect_mapper = self.mapper_factory.create_mapper(EffectRecord)
        self.action_mapper = self.mapper_factory.create_mapper(ActionRecord)
        self.level_mapper = self.mapper_factory.create_mapper(LevelRecord)


        self.num_effects = 10
        self.num_actions = 10
        self.num_levels = 10
        self.effects = frozenset(map(effect_from_num, range(self.num_effects)))
        repeat_effects = itertools.repeat(tuple(self.effects))
        self.actions = frozenset(map(action_from_num, range(self.num_actions), repeat_effects, repeat_effects))
        repeat_actions = itertools.repeat(tuple(self.actions))
        self.levels = frozenset(map(level_from_num, range(self.num_levels), repeat_actions))

    def test_read(self):
        self.create_all(self.effect_mapper, self.effects)
        self.create_all(self.action_mapper, self.actions)

        r = random.sample(self.levels, 1)[0]
        r_dict = r._asdict()
        key = r_dict['number']
        self.level_mapper.create(r)
        self.assertTupleEqual(r, self.level_mapper.read(key))

    def test_read_all(self):
        self.create_all(self.effect_mapper, self.effects)
        self.create_all(self.action_mapper, self.actions)
        self.create_all(self.level_mapper, self.levels)

        records = frozenset(self.level_mapper.read_all())
        self.assertEqual(records - self.levels, frozenset())

    def test_update(self):
        self.create_all(self.effect_mapper, self.effects)
        self.create_all(self.action_mapper, self.actions)
        r = random.sample(self.levels, 1)[0]
        self.level_mapper.create(r)
        key = r._asdict()['number']

        new_r = r._replace(experience=random.randint(0, 30000),
                           action_name=random.sample(self.actions, 1)[0]._asdict()['name'],
                           base_strength=random.randint(1, 30000),
                           base_agility=random.randint(10, 30000),
                           base_hp=random.randint(1, 30000),
                           base_mp=random.randint(1, 30000))
        self.level_mapper.update(new_r)
        self.assertTupleEqual(new_r, self.level_mapper.read(key))

    def test_delete(self):
        self.create_all(self.effect_mapper, self.effects)
        self.create_all(self.action_mapper, self.actions)
        self.create_all(self.level_mapper, self.levels)

        r = random.sample(self.levels, 1)[0]
        key = r._asdict()['number']

        self.level_mapper.delete(key)
        self.assertRaises(RecordMapper.NoSuchRecord, self.level_mapper.read, key)