# -*- coding: utf-8 -*-
__author__ = 'agates'

import random

from pythonwarrior.databases.interface import EquipmentTypeRecord, RecordMapper

from test_base import BaseTestCase


class EquipmentTypeRecordTestCase(BaseTestCase):
    def setUp(self):
        def equipment_type_from_num(num):
            return EquipmentTypeRecord('equipment_type{0}'.format(num))

        self.baseSetUp()
        self.equipment_type_mapper = self.mapper_factory.create_mapper(EquipmentTypeRecord)

        self.num_equipment_types = 10
        self.equipment_types = frozenset(map(equipment_type_from_num, range(self.num_equipment_types)))

    def test_read(self):
        r = random.sample(self.equipment_types, 1)[0]
        key = r._asdict()['name']
        self.equipment_type_mapper.create(r)
        self.assertTupleEqual(r, self.equipment_type_mapper.read(key))

    def test_read_all(self):
        self.create_all(self.equipment_type_mapper, self.equipment_types)

        records = frozenset(self.equipment_type_mapper.read_all())

        self.assertEqual(records - self.equipment_types, frozenset())

    def test_update(self):
        r = random.sample(self.equipment_types, 1)[0]
        self.equipment_type_mapper.create(r)
        key = r._asdict()['name']

        #new_r = r._replace(code='my_code')
        self.equipment_type_mapper.update(r)

        # Update on this record is useless
        # This code is still here in case we want to expand it later
        self.assertTupleEqual(r, self.equipment_type_mapper.read(key))

    def test_delete(self):
        self.create_all(self.equipment_type_mapper, self.equipment_types)

        r = random.sample(self.equipment_types, 1)[0]
        key = r._asdict()['name']

        self.equipment_type_mapper.delete(key)
        self.assertRaises(RecordMapper.NoSuchRecord, self.equipment_type_mapper.read, key)