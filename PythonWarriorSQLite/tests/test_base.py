# -*- coding: utf-8 -*-
__author__ = 'agates'

import os
from unittest import TestCase

from pythonwarrior.databases.sqlite import *


class BaseTestCase(TestCase):
    @staticmethod
    def create_all(mapper, records):
        for r in records:
            mapper.create(r)

    def baseSetUp(self):
        self.mapper_factory = SQLiteMapperFactory()
        schema = '../schema.sql' if os.path.isfile('../schema.sql') else 'PythonWarriorSQLite/schema.sql'
        with open(schema) as query:
            self.mapper_factory.conn.executescript(query.read())

    @abstractmethod
    def setUP(self):
        pass

    @abstractmethod
    def test_read(self):
        pass

    @abstractmethod
    def test_read_all(self):
        pass

    @abstractmethod
    def test_update(self):
        pass

    @abstractmethod
    def test_delete(self):
        pass