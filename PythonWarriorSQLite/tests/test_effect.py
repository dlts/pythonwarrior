# -*- coding: utf-8 -*-
__author__ = 'agates'

import random

from pythonwarrior.databases.interface import EffectRecord, RecordMapper

from test_base import BaseTestCase


class EffectRecordTestCase(BaseTestCase):
    def setUp(self):
        def effect_from_num(num):
            return EffectRecord('test_name{0}'.format(num), 'test_code{0}'.format(num))

        self.baseSetUp()
        self.effect_mapper = self.mapper_factory.create_mapper(EffectRecord)

        self.num_effects = 10
        self.effects = frozenset(map(effect_from_num, range(self.num_effects)))

    def test_read(self):
        r = random.sample(self.effects, 1)[0]
        key = r._asdict()['name']
        self.effect_mapper.create(r)
        self.assertTupleEqual(r, self.effect_mapper.read(key))

    def test_read_all(self):
        self.create_all(self.effect_mapper, self.effects)

        records = frozenset(self.effect_mapper.read_all())

        self.assertEqual(records - self.effects, frozenset())

    def test_update(self):
        r = random.sample(self.effects, 1)[0]
        self.effect_mapper.create(r)
        key = r._asdict()['name']

        new_r = r._replace(code='my_code')
        self.effect_mapper.update(new_r)

        self.assertTupleEqual(new_r, self.effect_mapper.read(key))

    def test_delete(self):
        self.create_all(self.effect_mapper, self.effects)

        r = random.sample(self.effects, 1)[0]
        key = r._asdict()['name']

        self.effect_mapper.delete(key)
        self.assertRaises(RecordMapper.NoSuchRecord, self.effect_mapper.read, key)