# -*- coding: utf-8 -*-
__author__ = 'agates'

import itertools
import random

from pythonwarrior.databases.interface import ActionRecord, EffectRecord, RecordMapper

from test_base import BaseTestCase


class ActionRecordTestCase(BaseTestCase):
    def setUp(self):
        def effect_from_num(num):
            return EffectRecord('test_name{0}'.format(num), 'test_code{0}'.format(num))

        def action_from_num(num, actor_effects, target_effects):
            return ActionRecord('action_name{0}'.format(num), 'action_label{0}'.format(num),
                                frozenset(r[0] for r in actor_effects), frozenset(r[0] for r in target_effects))

        self.baseSetUp()
        self.effect_mapper = self.mapper_factory.create_mapper(EffectRecord)
        self.action_mapper = self.mapper_factory.create_mapper(ActionRecord)

        self.num_effects = 10
        self.num_actions = 10
        self.effects = frozenset(map(effect_from_num, range(self.num_effects)))
        repeat_effects = itertools.repeat(tuple(self.effects))
        self.actions = frozenset(map(action_from_num, range(self.num_actions), repeat_effects, repeat_effects))

    def test_read(self):
        self.create_all(self.effect_mapper, self.effects)

        r = random.sample(self.actions, 1)[0]
        r_dict = r._asdict()
        key = r_dict['name']
        self.action_mapper.create(r)
        self.assertTupleEqual(r, self.action_mapper.read(key))

    def test_read_all(self):
        self.create_all(self.effect_mapper, self.effects)
        self.create_all(self.action_mapper, self.actions)

        records = frozenset(self.action_mapper.read_all())
        self.assertEqual(records - self.actions, frozenset())

    def test_update(self):
        self.create_all(self.effect_mapper, self.effects)
        r = random.sample(self.actions, 1)[0]
        self.action_mapper.create(r)
        key = r._asdict()['name']

        # Since the underlying SQL uses subtables, if we delete effects
        # from this record, the SQL needs to follow through with those changes
        new_r = r._replace(label='my_new_label',
                           actor_effect_names=frozenset(r[0] for r in random.sample(self.effects, 5)),
                           target_effect_names=frozenset(r[0] for r in random.sample(self.effects, 5)))
        self.action_mapper.update(new_r)
        self.assertTupleEqual(new_r, self.action_mapper.read(key))

        # Now make sure update adds back new effects
        new_r = r._replace(actor_effect_names=frozenset(r[0] for r in self.effects),
                           target_effect_names=frozenset(r[0] for r in self.effects))
        self.action_mapper.update(new_r)
        self.assertTupleEqual(new_r, self.action_mapper.read(key))

    def test_delete(self):
        self.create_all(self.effect_mapper, self.effects)
        self.create_all(self.action_mapper, self.actions)

        r = random.sample(self.actions, 1)[0]
        key = r._asdict()['name']

        self.action_mapper.delete(key)
        self.assertRaises(RecordMapper.NoSuchRecord, self.action_mapper.read, key)