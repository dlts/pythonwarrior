# -*- coding: utf-8 -*-
__author__ = 'agates'

import itertools
import random

from pythonwarrior.databases.interface import EquipmentRecord, EquipmentTypeRecord, EffectRecord, RecordMapper

from test_base import BaseTestCase


class EquipmentRecordTestCase(BaseTestCase):
    def setUp(self):
        def effect_from_num(num):
            return EffectRecord('test_name{0}'.format(num), 'test_code{0}'.format(num))

        def equipment_type_from_num(num):
            return EquipmentTypeRecord('equipment_type{0}'.format(num))

        def equipment_from_num(num, equipment_types, effects):
            return EquipmentRecord('equipment_name{0}'.format(num),
                                   random.sample(equipment_types, 1)[0]._asdict()['name'],
                                   random.randint(0, 30000),
                                   frozenset(r[0] for r in effects))

        self.baseSetUp()
        self.effect_mapper = self.mapper_factory.create_mapper(EffectRecord)
        self.equipment_type_mapper = self.mapper_factory.create_mapper(EquipmentTypeRecord)
        self.equipment_mapper = self.mapper_factory.create_mapper(EquipmentRecord)

        self.num_effects = 10
        self.num_equipment_type = 10
        self.num_equipment = 100
        self.effects = frozenset(map(effect_from_num, range(self.num_effects)))
        repeat_effects = itertools.repeat(tuple(self.effects))
        self.equipment_types = frozenset(map(equipment_type_from_num, range(self.num_equipment_type)))
        repeat_equipment_type = itertools.repeat(tuple(self.equipment_types))
        self.equipment = frozenset(map(equipment_from_num, range(self.num_equipment), repeat_equipment_type, repeat_effects))

    def test_read(self):
        self.create_all(self.effect_mapper, self.effects)
        self.create_all(self.equipment_type_mapper, self.equipment_types)

        r = random.sample(self.equipment, 1)[0]
        r_dict = r._asdict()
        key = r_dict['name']
        self.equipment_mapper.create(r)
        self.assertTupleEqual(r, self.equipment_mapper.read(key))

    def test_read_all(self):
        self.create_all(self.effect_mapper, self.effects)
        self.create_all(self.equipment_type_mapper, self.equipment_types)
        self.create_all(self.equipment_mapper, self.equipment)

        records = frozenset(self.equipment_mapper.read_all())
        self.assertEqual(records - self.equipment, frozenset())

    def test_update(self):
        self.create_all(self.effect_mapper, self.effects)
        self.create_all(self.equipment_type_mapper, self.equipment_types)
        r = random.sample(self.equipment, 1)[0]
        self.equipment_mapper.create(r)
        key = r._asdict()['name']

        # Since the underlying SQL uses subtables, if we delete effects
        # from this record, the SQL needs to follow through with those changes
        new_r = r._replace(equipment_type_name=random.sample(self.equipment_types, 1)[0]._asdict()['name'],
                           power=random.randint(0, 30000),
                           step_effect_names=frozenset(r[0] for r in random.sample(self.effects, 5)))
        self.equipment_mapper.update(new_r)
        self.assertTupleEqual(new_r, self.equipment_mapper.read(key))

        # Now make sure update adds back new effects
        new_r = r._replace(step_effect_names=frozenset(r[0] for r in self.effects))
        self.equipment_mapper.update(new_r)
        self.assertTupleEqual(new_r, self.equipment_mapper.read(key))

    def test_delete(self):
        self.create_all(self.effect_mapper, self.effects)
        self.create_all(self.equipment_type_mapper, self.equipment_types)
        self.create_all(self.equipment_mapper, self.equipment)

        r = random.sample(self.equipment, 1)[0]
        key = r._asdict()['name']

        self.equipment_mapper.delete(key)
        self.assertRaises(RecordMapper.NoSuchRecord, self.equipment_mapper.read, key)