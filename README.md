## [Travis Continuous Integration Build] (http://travis-ci.org/wkmanire/pythonwarrior)

![Travis-CI Build](https://travis-ci.org/wkmanire/pythonwarrior.svg?branch=master)

Tested With:
* cPython 2.7
* cPython 3.4
* PyPy 2.2.1


__The build may be passing, but this library is still extremely unstable.__

I would advise against building anything with pythonwarrior until it has
reached version 1.0.0 which will mark the first feature complete release
of the library.

## Python Warrior

A simulator that captures all of the __intended__ behaviors of "Dragon Warrior"
for the Nintendo Entertainment System originally published by Enix in
1986.

### Bugs

Note, that I use the word __intended__. There are some known bugs with
the original system that were not ported into this simulation. I do not believe
that these bugs significantly impact the game-play experience. It would have
required specific programming to reintroduce them and that seems like an
inherently incorrect thing to do.

#### Known Bugs Not Ported:

1. The Chest Counter Bug
1. The Fighter's Ring Bug

## Documentation Credits

I have used as a reference several excellent documents which I've downloaded
from http://gamefaqs.com. They are included in their entirety and unaltered in
the projects under the docs directory.

* Dragon Warrior (NES) Formulas v1.0, By Ryan8Bit <generalkillt@yahoo.com>
* Akira Slime's Guide To: Name, Stats, and Levels for DRAGON WARRIOR, Version 1.0 By Akira Speirs
* DRAGON WARRIOR FAQ, v1.4 by Clasher

## Additional Credits

I own a copy of the original cartridge of the American release of the game
which I've used at length in order to verify the operation of this simulation
library.

I also own a copy of the original player's manual, which was distributed with
the cartridge, that I also used as a reference.

## To Enix

Thank you for the game that spawned a genre which I have enjoyed so much for so
many years.
