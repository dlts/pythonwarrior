from setuptools import setup

setup(
    name='PythonWarrior Core Library',
    version='',
    packages=["pythonwarrior", "pythonwarrior.databases", "pythonwarrior.simulation"],
    namespaces=["pythonwarrior", "pythonwarrior.databases"],
    namespace_packages=["pythonwarrior.databases"],
    url='',
    license='',
    author='wkmanire',
    author_email='williamkmanire@gmail.com',
    description='',
)
