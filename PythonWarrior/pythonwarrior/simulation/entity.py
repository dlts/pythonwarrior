import random


class Entity(object):

    def __init__(self):
        self.actions = dict()
        self.strength = 0
        self.agility = 0
        self.hp = 0
        self.max_hp = 0
        self.last_damage_taken = 0
        self.can_preemptively_flee = False

    def restore_hp(self, amount):
        self.hp = min(self.max_hp, amount + self.hp)

    def take_damage(self, amount):
        self.last_damage_taken = self.hp - max(self.hp - amount, 0)
        self.hp -= self.last_damage_taken

    def defend_against_spell(self, minimum_damage, maximum_damage):
        self.take_damage(random.randint(minimum_damage, maximum_damage))

    @property
    def is_dead(self):
        return self.hp == 0

    @property
    def attack_power(self):
        return self.strength

    @property
    def defense_power(self):
        return self.agility

    def attack(self, victim):
        victim.defend_against_melee(self.attack_power)

    def defend_against_melee(self, enemy_attack_power):
        minimum = (enemy_attack_power - self.defense_power // 2) // 4
        maximum = (enemy_attack_power - self.defense_power // 2) // 2
        self.take_damage(random.randint(minimum, maximum))

    def add_action(self, action):
        self.actions[action.name] = action

    def act(self, action_name, target_entity):
        self.actions[action_name].act(self, target_entity)

    def preemptively_flee(self):
        return False

    def wins_initiative(self, other_entity):
        return True

    def reward_entity(self, other_entity):
        pass