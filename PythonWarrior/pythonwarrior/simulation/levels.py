import math


class StatTypes(object):
    STRENGTH = 1
    AGILITY = 2
    HP = 4
    MP = 8
    ALL_TYPES = [STRENGTH, AGILITY, HP, MP]


class Level(object):

    def __init__(self, number, experience, spell):
        self.next_level = None
        self.previous_level = None
        self.experience = experience
        self.spell = spell
        self.number = number
        self.base_agility = 0
        self.base_strength = 0
        self.base_hp = 0
        self.base_mp = 0
        self.base_stat_by_stat_type = {StatTypes.STRENGTH: "base_strength",
                                       StatTypes.AGILITY: "base_agility",
                                       StatTypes.HP: "base_hp",
                                       StatTypes.MP: "base_mp"}

    def get_adjusted_strength(self, hero_name):
        return self.get_adjusted_stat(hero_name, StatTypes.STRENGTH)

    def get_adjusted_agility(self, hero_name):
        return self.get_adjusted_stat(hero_name, StatTypes.AGILITY)

    def get_adjusted_hp(self, hero_name):
        return self.get_adjusted_stat(hero_name, StatTypes.HP)
    
    def get_adjusted_mp(self, hero_name):
        return self.get_adjusted_stat(hero_name, StatTypes.MP)

    def get_adjusted_stat(self, hero_name, stat_type):
        if hero_name.get_growth_type() & stat_type:
            return self.get_stat_by_stat_type(stat_type)
        else:
            return self.adjust_stat(hero_name, self.get_stat_by_stat_type(stat_type))

    def get_stat_by_stat_type(self, stat_type):
        return getattr(self, self.base_stat_by_stat_type[stat_type])

    def adjust_stat(self, hero_name, stat):
        offset = (hero_name.get_name_sum() // 4) % 4
        return math.floor(stat * (9.0 / 10.0)) + offset

    def get_spells(self):
        spells = list()
        ptr = self
        while ptr is not None:
            if ptr.spell is not None:
                spells.append(ptr.spell)
            ptr = ptr.previous_level
        spells.reverse()
        return spells

    def __repr__(self):
        return "Level(%d)" % self.number

