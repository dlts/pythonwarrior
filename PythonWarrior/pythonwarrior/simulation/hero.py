import random
import re

from pythonwarrior.simulation.entity import Entity
from pythonwarrior.simulation.levels import StatTypes
from pythonwarrior.simulation.inventory import Inventory


class GrowthTypes(object):
    HP_AND_MP = StatTypes.HP | StatTypes.MP
    STRENGTH_AND_HP = StatTypes.STRENGTH | StatTypes.HP
    AGILITY_AND_MP = StatTypes.AGILITY | StatTypes.MP
    STRENGTH_AND_AGILITY = StatTypes.STRENGTH | StatTypes.AGILITY

    ALL_TYPES = [HP_AND_MP, STRENGTH_AND_HP,
                 AGILITY_AND_MP, STRENGTH_AND_AGILITY]


class Hero(Entity):

    class InsufficientMPException(Exception):
        pass

    def __init__(self, hero_name, inventory, level):
        super(Hero, self).__init__()
        self.name = hero_name
        self.level = level
        self.experience = 0
        self.mp = 0
        self.inventory = inventory
        self.ring = NoWeapon()
        self.weapon = NoWeapon()
        self.armor = NoBodyArmor()
        self.shield = NoArmor()
        self.accessory = NoArmor()

    @property
    def strength(self):
        return self.level.get_adjusted_strength(self.name)

    @strength.setter
    def strength(self, value):
        pass

    @property
    def agility(self):
        return self.level.get_adjusted_agility(self.name)

    @agility.setter
    def agility(self, value):
        pass

    @property
    def max_hp(self):
        return self.level.get_adjusted_hp(self.name)

    @max_hp.setter
    def max_hp(self, value):
        pass

    @property
    def max_mp(self):
        return self.level.get_adjusted_mp(self.name)

    def restore_mp(self, amount):
        self.mp += min(self.max_mp, self.mp + amount)

    def spend_mp(self, amount):
        if self.mp - amount < 0:
            raise self.InsufficientMPException()
        self.mp -= amount

    def take_item(self, item):
        self.inventory.add_item(item)

    def use_item(self, item_name):
        item = self.inventory.remove_item(item_name)
        item.use(self)

    def equip_armor(self, armor):
        self.armor = armor

    def equip_shield(self, shield):
        self.shield = shield

    def wear_ring(self, ring):
        self.ring = ring

    def wear_accessory(self, accessory):
        self.accessory = accessory

    def wield(self, weapon):
        self.weapon = weapon

    def defend_against_spell(self, minimum_damage, maximum_damage):
        amount = random.randint(minimum_damage, maximum_damage)
        final_amount = self.armor.defend_against_spell(amount)
        self.take_damage(final_amount)

    def step(self):
        self.armor.apply_step_effect(self)

    @property
    def attack_power(self):
        base = super(Hero, self).attack_power
        with_weapon = self.weapon.add_attack_power(base)
        with_weapon_and_ring = self.ring.add_attack_power(with_weapon)
        return with_weapon_and_ring

    def get_all_armor(self):
        return [self.armor, self.shield, self.accessory]

    @property
    def defense_power(self):
        amount = super(Hero, self).defense_power
        for armor in self.get_all_armor():
            amount = armor.add_defense_power(amount)
        return amount

    def gain_experience(self, experience):
        self.experience += experience
        while self.level.next_level is not None and self.experience >= self.level.next_level.experience:
            self.level = self.level.next_level

    def get_experience_to_level(self):
        if self.level.next_level is None:
            return 0
        else:
            return self.level.next_level.experience - self.experience


class HeroName(object):

    VALID_NAME_REGEX = re.compile(r"^[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.,-?!\)\(]*$")
    LETTER_VALUE_TABLE = {"a": 10, "b": 11, "c": 12, "d": 13, "e": 14, "f": 15, "g": 0, "h": 1, "i": 2, "j": 3, "k": 4,
                          "l": 5, "m": 6, "n": 7, "o": 8, "p": 9, "q": 10, "r": 11, "s": 12, "t": 13, "u": 14, "v": 15,
                          "w": 0, "x": 1, "y": 2, "z": 3, "A": 4, "B": 5, "C": 6, "D": 7, "E": 8, "F": 9, "G": 10,
                          "H": 11, "I": 12, "J": 13, "K": 14, "L": 15, "M": 0, "N": 1, "O": 2, "P": 3, "Q": 4, "R": 5,
                          "S": 6, "T": 7, "U": 8, "V": 9, "W": 10, "X": 11, "Y": 12, "Z": 13, "'": 0, ".": 7, ",": 8,
                          "-": 9, "?": 11, "!": 12, ")": 14, "(": 15}

    def __init__(self, name):
        self.name = name

    def get_name_sum(self):
        return sum(map(self.get_letter_value, self.name[0:4]))

    def get_letter_value(self, letter):
        return self.LETTER_VALUE_TABLE[letter]

    def get_growth_type(self):
        return GrowthTypes.ALL_TYPES[self.get_name_sum() % 4]

    def validate(self):
        return self.VALID_NAME_REGEX.match(self.name)


class Weapon(object):

    def __init__(self):
        self.name = None

    def add_attack_power(self, amount):
        return amount


class NoWeapon(Weapon):

    def __init__(self):
        super(NoWeapon, self).__init__()
        self.name = "No Weapon"


class Armor(object):

    def __init__(self):
        self.name = None

    def add_defense_power(self, amount):
        return amount


class BodyArmor(Armor):

    def __init__(self):
        super(BodyArmor, self).__init__()

    def defend_against_spell(self, amount):
        return amount

    def apply_step_effect(self, hero):
        pass


class NoBodyArmor(BodyArmor):

    def __init__(self):
        super(NoBodyArmor, self).__init__()
        self.name = "No Body Armor"


class NoArmor(Armor):

    def __init__(self):
        super(NoArmor, self).__init__()
        self.name = "No Armor"


class HeroFactory(object):

    class MissingLevelInteractor(Exception):
        pass

    def __init__(self):
        self.level_interactor = None

    def create_hero(self, hero_name):
        self.ensure_level_interactor()
        level = self.level_interactor.load_levels()
        return Hero(HeroName(hero_name), Inventory(), level)

    def ensure_level_interactor(self):
        if self.level_interactor is None:
            raise self.MissingLevelInteractor

