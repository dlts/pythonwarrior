# -*- coding: utf-8 -*-

from pythonwarrior.simulation.actions import Effect


class Inventory(object):

    MAX_INVENTORY_SIZE = 10
    MAX_STACK_SIZE = 6

    class InventoryFullError(Exception):
        pass

    def __init__(self):
        self.items = list()

    def __len__(self):
        return len(self.items)

    def add_items(self, *items):
        for item in items:
            self.add_item(item)

    def add_item(self, item):
        if self.is_full():
            raise self.InventoryFullError("Inventory is limited to 10 slots")
        if item.is_stackable and self.has_item(item.name):
            self.add_stackable_item(item)
        else:
            self.items.append([item, 1])

    def is_full(self):
        return len(self) == self.MAX_INVENTORY_SIZE

    def add_stackable_item(self, item):
        idx = self.get_item_index_by_name(item.name)
        if self.stack_is_full_at_index(idx):
            raise self.InventoryFullError("Cannot carry more than 6 of that type of item.")
        else:
            self.items[idx][1] += 1

    def get_item_index_by_name(self, search_name):
        for idx, pair in enumerate(self.items):
            item, _ = pair
            if item.name == search_name:
                return idx
        raise KeyError

    def stack_is_full_at_index(self, idx):
        return self.items[idx][1] == self.MAX_STACK_SIZE

    def remove_item(self, item_name):
        idx = self.get_item_index_by_name(item_name)
        item_qty_pair = self.items[idx]
        item, qty = item_qty_pair
        if qty > 1:
            self.items[idx][1] -= 1
        else:
            self.items.remove(item_qty_pair)
        return item

    def has_item(self, name):
        for item, qty in self.items:
            if item.name == name:
                return True
        return False

    def get_quantity(self, name):
        for item, qty in self.items:
            if item.name == name:
                return qty
        raise KeyError("No item named %s found in inventory." % name)

    def get_report(self):
        return [(item.name, qty) for item, qty in self.items]


class Item(object):

    def __init__(self, name, effect):
        self.name = name
        self.effect = effect
        self.is_stackable = False

    def use(self, target):
        self.effect.execute(target)


class StackableItem(Item):

    def __init__(self, name, code_string):
        super(StackableItem, self).__init__(name, code_string)
        self.is_stackable = True