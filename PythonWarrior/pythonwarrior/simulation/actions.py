from abc import ABCMeta, abstractmethod


class Action(object):

    def __init__(self, name):
        self.name = name
        self.subject_effects = list()
        self.target_effects = list()

    def act(self, subject, target):
        self.execute_effects(self.subject_effects, subject)
        self.execute_effects(self.target_effects, target)

    @staticmethod
    def execute_effects(modifiers, target):
        for modifier in modifiers:
            modifier.execute(target)

    def get_subject_effects(self):
        return self.subject_effects

    def add_subject_effect(self, modifier):
        self.subject_effects.append(modifier)

    def get_target_effects(self):
        return self.target_effects

    def add_target_effect(self, modifier):
        self.target_effects.append(modifier)


class Effect(object):

    @staticmethod
    def NOOP(target):
        pass

    def __init__(self, name, code_string):
        self.name = name
        self.code = code_string
        self.effect_fn = self.get_effect_fn()

    def get_effect_fn(self):
        globals_container = dict()
        exec(self.code, globals_container)
        return globals_container.get("effect", self.NOOP)

    def execute(self, target):
        return self.effect_fn(target)
