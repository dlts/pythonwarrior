

class Battle(object):

    class InvalidTargetException(Exception):
        pass

    def __init__(self, entity_a, entity_b):
        self.state = BattleStates.PRE_BATTLE
        self.state_handlers = {
            BattleStates.PRE_BATTLE: self.on_pre_battle,
            BattleStates.GET_ACTION: self.on_get_action,
            BattleStates.EXECUTE_ACTION: self.on_execute_action,
            BattleStates.POST_BATTLE: lambda: None
        }
        self.entity_a = entity_a
        self.entity_b = entity_b
        self.acting_entity = None
        self.action_name = None
        self.target = None

    def update(self):
        self.state_handlers[self.state]()

    def on_pre_battle(self):
        if self.entity_a.preemptively_flee(self.entity_b) or self.entity_b.preemptively_flee(self.entity_a):
            self.state = BattleStates.POST_BATTLE
        else:
            self.decide_initiative()
            self.state = BattleStates.GET_ACTION

    def decide_initiative(self):
        if self.entity_a.wins_initiative(self.entity_b):
            self.acting_entity = self.entity_a
        else:
            self.acting_entity = self.entity_b

    def on_get_action(self):
        if self.action_name is not None and self.target is not None:
            self.state = BattleStates.EXECUTE_ACTION

    def get_victor_and_loser(self):
        if self.entity_a.is_dead():
            return self.entity_b, self.entity_a
        elif self.entity_b.is_dead():
            return self.entity_a, self.entity_b
        else:
            return None, None

    def on_execute_action(self):
        self.acting_entity.act(self.action_name, self.target)
        victor, loser = self.get_victor_and_loser()
        if victor is not None:
            loser.reward_entity(victor)
            self.state = BattleStates.POST_BATTLE
        else:
            self.swap_acting_entity()
            self.state = BattleStates.GET_ACTION

    def swap_acting_entity(self):
        if self.acting_entity is self.entity_a:
            self.acting_entity = self.entity_b
        else:
            self.acting_entity = self.entity_a

    def set_action_name(self, action_name):
        self.action_name = action_name

    def set_target(self, target):
        if target is not self.entity_a and target is not self.entity_b:
            raise self.InvalidTargetException()
        self.target = target

    def get_acting_entity(self):
        return self.acting_entity

    def is_over(self):
        return self.state == BattleStates.POST_BATTLE


class BattleStates(object):
    PRE_BATTLE = 0
    GET_ACTION = 1
    EXECUTE_ACTION = 2
    POST_BATTLE = 3
