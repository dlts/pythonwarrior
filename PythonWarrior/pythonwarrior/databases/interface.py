from abc import ABCMeta, abstractmethod
from collections import namedtuple


class RecordMapper(object):  # pragma: no cover

    __meta__ = ABCMeta

    class NoSuchRecord(Exception):
        pass

    class DuplicateRecord(Exception):
        pass

    @abstractmethod
    def create(self, record):
        pass

    @abstractmethod
    def read(self, key):
        pass

    @abstractmethod
    def read_all(self):
        pass

    @abstractmethod
    def update(self, record):
        pass

    @abstractmethod
    def delete(self, primary_key):
        pass


class MapperFactory(object):  # pragma: no cover
    """
    All mapper factories should inherit MapperFactory and override create_mapper to
    return mapper instances.
    """

    __meta__ = ABCMeta

    @abstractmethod
    def create_mapper(self, record_type):
        """
        Given one of the record types specified in pythonwarrior.databases.interface return a RecordMapper
        instance for that type.
        :param record_type: A record type from pythonwarrior.databases.interface
        :return: A RecordMapper instance
        """
        pass


EffectRecord = namedtuple(
    "EffectRecord",
    ["name", "code"]
)

ActionRecord = namedtuple(
    "ActionRecord",
    ["name", "label", "actor_effect_names", "target_effect_names"]
)

LevelRecord = namedtuple(
    "LevelRecord",
    ["number", "experience", "action_name", "base_strength", "base_agility", "base_hp", "base_mp"]
)

HeroRecord = namedtuple(
    "HeroRecord",
    ["name", "experience", "hp", "mp", "ring_name", "weapon_name",
     "armor_name", "shield_name", "accessory_name"]
)

EquipmentTypeRecord = namedtuple(
    "EquipmentTypeRecord",
    ["name"]
)

EquipmentRecord = namedtuple(
    "EquipmentRecord",
    ["name", 'equipment_type_name', "power", "step_effect_names"]
)

InventoryEntryRecord = namedtuple(
    "InventoryEntryRecord",
    ["hero_name", "item_name",  "quantity"]
)

ItemRecord = namedtuple(
    "ItemRecord",
    ["name", "is_stackable", "effect_name"]
)

SpellRecord = namedtuple(
    "SpellRecord",
    ["name", "action_name"]
)

MonsterRecord = namedtuple(
    "MonsterRecord",
    ["name", "strength", "agility", "max_hp", "map_mp", "gold", "experience", "ai_effect_name", "action_names"]
)

AIEffectRecord = namedtuple(
    "AIRecord",
    ["name", "code"]
)

