from unittest import TestCase

from mock import Mock, patch

from pythonwarrior.simulation.actions import Effect, Action
from pythonwarrior.simulation.entity import Entity


class EntityData(object):
    pass


class EntityTestCase(TestCase):

    def setUp(self):
        self.entity = Entity()
        self.entity.max_hp = 10
        self.entity.restore_hp(self.entity.max_hp)

    def test_when_entity_restores_hp_its_hp_is_increased(self):
        self.assertEqual(self.entity.hp, self.entity.max_hp)

    def test_when_restore_hp_amount_exceeds_max_hp_it_is_limited_to_max_hp(self):
        self.entity.restore_hp(10)
        self.assertEqual(self.entity.hp, self.entity.max_hp)

    def test_when_entity_takes_damage_its_hp_is_reduced(self):
        self.entity.take_damage(5)
        self.assertEqual(self.entity.hp, 5)

    def test_when_entity_takes_more_damage_than_remaining_hp_it_becomes_zero(self):
        self.entity.take_damage(1000)
        self.assertEqual(0, self.entity.hp)

    def test_when_hp_is_0_is_dead_returns_true(self):
        self.entity.take_damage(self.entity.max_hp)
        self.assertTrue(self.entity.is_dead)

    def test_when_one_entity_attacks_another_the_victim_calculates_the_damage(self):
        victim = Mock(Entity)
        self.entity.attack(victim)
        victim.defend_against_melee.assert_called_once_with(self.entity.attack_power)

    def test_attack_power_is_by_default_strength(self):
        self.assertEqual(self.entity.attack_power, self.entity.strength)

    def test_defense_power_is_by_default_agility(self):
        self.assertEqual(self.entity.defense_power, self.entity.agility)

    @patch("random.randint", return_value=2)
    def test_defend_against_melee_calculates_and_applies_damage(self, randint):
        self.entity.agility = 4
        self.entity.defend_against_melee(10)
        randint.assert_called_once_with(2, 4)
        self.assertEqual(self.entity.hp, 8)

    @patch("random.randint", return_value=10)
    def test_defend_against_spell_damage_applies_full_damage_by_default(self, randint):
        self.entity.defend_against_spell(5, 10)
        self.assertTrue(self.entity.is_dead)
        randint.assert_called_once_with(5, 10)

    def test_entity_keeps_track_of_last_damage_taken(self):
        self.assertEqual(0, self.entity.last_damage_taken)
        self.entity.take_damage(3)
        self.assertEqual(3, self.entity.last_damage_taken)
        self.entity.take_damage(2)
        self.assertEqual(2, self.entity.last_damage_taken)
        self.assertEqual(5, self.entity.hp)

    def test_entity_has_actions_that_can_be_enacted_by_name(self):
        mock_target_effect, mock_subject_effect = Mock(Effect), Mock(Effect)
        action = Action("Test")
        action.add_subject_effect(mock_subject_effect)
        action.add_target_effect(mock_target_effect)
        self.entity.add_action(action)
        self.entity.act("Test", self.entity)
        mock_target_effect.execute.assert_called_once_with(self.entity)
        mock_subject_effect.execute.assert_called_once_with(self.entity)

    def test_by_default_preemptively_flee_returns_False(self):
        self.assertFalse(Entity().preemptively_flee())

    def test_by_default_wins_initiative_returns_True(self):
        self.assertTrue(Entity().wins_initiative(None))

    def test_by_default_reward_entity_has_no_effect(self):
        self.assertIsNone(Entity().reward_entity(None))
