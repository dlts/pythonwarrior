from unittest import TestCase

from mock import Mock

from pythonwarrior.simulation.inventory import Item, StackableItem, Inventory
from pythonwarrior.simulation.actions import Effect


class InventoryTestCase(TestCase):
    def setUp(self):
        self.inv = Inventory()

    def test_add_item_non_stackable_items(self):
        i1 = Item("Dragon Scale", "")
        i2 = Item("Dragon Scale", "")
        self.inv.add_item(i1)
        self.inv.add_item(i2)
        self.assertEqual(len(self.inv), 2)

    def test_add_stackable_items(self):
        self.inv.add_item(StackableItem("Torch", ""))
        self.inv.add_item(StackableItem("Torch", ""))
        self.inv.add_item(StackableItem("Key", ""))
        self.inv.add_item(StackableItem("Key", ""))
        self.inv.add_item(Item("Dragon Scale", ""))
        self.assertEqual(len(self.inv), 3)
        self.assertEqual(2, self.inv.get_quantity("Key"))
        self.assertEqual(1, self.inv.get_quantity("Dragon Scale"))

    def test_get_quantity_raises_key_error(self):
        with self.assertRaises(KeyError):
            self.inv.get_quantity("Herb")

    def test_has_item(self):
        self.assertFalse(self.inv.has_item("Herb"))
        self.inv.add_item(StackableItem("Herb", ""))
        self.assertTrue(self.inv.has_item("Herb"))
        self.assertFalse(self.inv.has_item("Fork"))

    def test_remove_item_raises_if_item_not_found(self):
        with self.assertRaises(KeyError):
            self.inv.remove_item("Foo")
        self.inv.add_item(StackableItem("Fork", ""))
        with self.assertRaises(KeyError):
            self.inv.remove_item("Foo")

    def test_remove_item_removes_item_from_inventory_if_qty_zero(self):
        self.inv.add_item(StackableItem("Herb", ""))
        self.inv.remove_item("Herb")
        self.assertFalse(self.inv.has_item("Herb"))

    def test_remove_item_reduces_quantity(self):
        self.inv.add_item(StackableItem("Herb", ""))
        self.inv.add_item(StackableItem("Herb", ""))
        self.inv.add_item(StackableItem("Herb", ""))
        self.assertEqual(1, len(self.inv))
        self.assertEqual(3, self.inv.get_quantity("Herb"))
        self.inv.remove_item("Herb")
        self.inv.remove_item("Herb")
        self.assertEqual(1, self.inv.get_quantity("Herb"))
        self.assertEqual(1, len(self.inv))

    def test_remove_item_returns_an_item_by_the_same_name(self):
        i = StackableItem("Herb", "")
        self.inv.add_item(i)
        self.assertEqual(i.name, self.inv.remove_item("Herb").name)

    def test_inventory_limited_to_ten_items(self):
        for i in range(10):
            self.inv.add_item(Item("Herb", ""))
        with self.assertRaises(Inventory.InventoryFullError):
            self.inv.add_item(Item("Herb", ""))

    def test_stackable_items_limited_to_qty_of_six(self):
        for i in range(6):
            self.inv.add_item(StackableItem("Herb", ""))
        with self.assertRaises(Inventory.InventoryFullError):
            self.inv.add_item(StackableItem("Herb", ""))

    def test_get_item_index_by_name_raises_if_item_not_found(self):
        with self.assertRaises(KeyError):
            self.inv.get_item_index_by_name("foobar")


class ItemTestCase(TestCase):
    def setUp(self):
        self.mock_effect = Mock(Effect)
        self.item = Item("Foo", self.mock_effect)

    def test_item_is_initialized_with_its_name(self):
        self.assertEqual(self.item.name, "Foo")

    def test_items_are_not_stackable(self):
        self.assertFalse(self.item.is_stackable)

    def test_when_item_is_used_it_executes_its_effect(self):
        self.item.use("target")
        self.mock_effect.execute.assert_called_once_with("target")


class StackableItemTestCase(TestCase):
    def test_stackable_items_are_stackable(self):
        item = StackableItem("foo", "")
        self.assertTrue(item.is_stackable)