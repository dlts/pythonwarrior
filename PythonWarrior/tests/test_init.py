from unittest import TestCase
import pythonwarrior
import tests

class LibTestCase(TestCase):

    def test_test_init_exists(self):
        # I need a nice way of telling the coverage tool not to cover certain files...
        self.assertIsNotNone(tests)

    def test_init_defines_version_number(self):
        self.assertIsNotNone(pythonwarrior.__version__)

