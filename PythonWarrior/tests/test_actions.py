from unittest import TestCase

from mock import Mock

from pythonwarrior.simulation.actions import Action, Effect


class ActionStatusModifiersTestCase(TestCase):

    def setUp(self):
        self.action = Action("Test")
        self.mock_subject_effect = Mock()
        self.mock_target_effect = Mock()
        self.action.add_subject_effect(self.mock_subject_effect)
        self.action.add_target_effect(self.mock_target_effect)

    def test_when_action_is_new_it_should_have_no_subject_effects(self):
        self.assertEqual(0, len(Action("Test").get_subject_effects()))

    def test_when_action_new_it_should_have_no_target_subject_effects(self):
        self.assertEqual(0, len(Action("Test").get_target_effects()))

    def test_when_subject_effects_modifiers_are_added_they_should_be_accessible_via_the_getter(self):
        self.assertEqual(1, len(self.action.get_subject_effects()))
        self.assertIs(self.mock_subject_effect, self.action.get_subject_effects()[0])

    def test_when_target_effects_are_added_they_should_be_accessible_via_the_getter(self):
        self.assertEqual(1, len(self.action.get_target_effects()))
        self.assertIs(self.mock_target_effect, self.action.get_target_effects()[0])

    def test_when_act_is_called_it_should_execute_any_subject_effects_on_the_subject(self):
        mock_subject = Mock()
        self.action.act(mock_subject, Mock())
        self.mock_subject_effect.execute.assert_called_once_with(mock_subject)

    def test_when_act_is_called_it_should_execute_any_target_effects_on_the_target(self):
        mock_target = Mock()
        self.action.act(Mock(), mock_target)
        self.mock_target_effect.execute.assert_called_once_with(mock_target)


class EffectTestCase(TestCase):

    def test_execute(self):
        code_string = 'def effect(t): t["foo"] = "bar"'

        e = Effect("foo", code_string)
        target = dict()
        e.execute(target)
        self.assertEqual(target["foo"], "bar")
