from unittest import TestCase, skip

from pythonwarrior.simulation.levels import Level
from pythonwarrior.simulation.hero import HeroName


class LevelTestCase(TestCase):

    def setUp(self):
        self.l = Level(1, 0, None)
        self.l.base_strength = 7
        self.l.base_agility = 8
        self.l.base_hp = 31
        self.l.base_mp = 16

    def test_level____repr__(self):
        self.assertEqual("Level(1)", str(self.l))

    def test_level_has_next(self):
        self.assertIsNone(self.l.next_level)

    def test_level_has_prev(self):
        self.assertIsNone(self.l.previous_level)

    def test_level_has_experience(self):
        self.assertEqual(0, self.l.experience)

    def test_level_has_a_spell(self):
        self.assertIsNone(self.l.spell)

    def test_get_spells_returns_empty_list_if_none_learned(self):
        self.assertEqual(self.l.get_spells(), list())

    def test_get_spells_returns_spell_if_not_none(self):
        self.l.spell = "EAT MORE"
        self.assertEqual(self.l.get_spells(), ["EAT MORE"])

    def test_get_spells_returns_all_spells_from_prev_chain(self):
        l1 = Level(1, 0, None)
        l1.spell = "foo"

        l2 = Level(2, 0, None)
        l1.next_level = l2
        l2.previous_level = l1
        
        l3 = Level(3, 0, None)
        l2.next_level = l3
        l3.previous_level = l2
        l3.spell = "bar"
        
        l4 = Level(4, 0, None)
        l3.next_level = l4
        l4.previous_level = l3
        l4.spell = "whiz"

        self.assertEqual(l4.get_spells(), ["foo", "bar", "whiz"])

    def test_level_has_base_stats(self):
        self.assertEqual(self.l.base_agility, 8)
        self.assertEqual(self.l.base_strength, 7)
        self.assertEqual(self.l.base_hp, 31)
        self.assertEqual(self.l.base_mp, 16)

    def test_get_adjusted_strength_strong_growth(self):
        self.assertEqual(self.l.get_adjusted_strength(HeroName("Kefka")), self.l.base_strength)

    def test_get_adjusted_strength_weak_growth(self):
        self.assertEqual(self.l.get_adjusted_strength(HeroName("M")), 6)

    def test_get_adjusted_agility_strong_growth(self):
        self.assertEqual(self.l.get_adjusted_agility(HeroName("Kefka")), self.l.base_agility)

    def test_get_adjusted_agility_weak_growth(self):
        self.assertEqual(self.l.get_adjusted_agility(HeroName("M")), 7)

    def test_get_adjusted_hp_strong_growth(self):
        self.assertEqual(self.l.get_adjusted_hp(HeroName("A")), self.l.base_hp)

    def test_get_adjusted_hp_weak_growth(self):
        self.assertEqual(self.l.get_adjusted_hp(HeroName("z")), 27)

    def test_get_adjusted_mp_strong_growth(self):
        self.assertEqual(self.l.get_adjusted_mp(HeroName("a")), self.l.base_mp)

    def test_get_adjusted_mp_weak_growth(self):
        self.assertEqual(self.l.get_adjusted_mp(HeroName("z")), 14)
