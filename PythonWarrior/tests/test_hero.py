from unittest import TestCase

from mock import Mock, patch

from pythonwarrior.simulation.hero import Hero, BodyArmor, NoBodyArmor, Weapon, NoWeapon, NoArmor, Armor, HeroName
from pythonwarrior.simulation.inventory import Inventory, Item
from pythonwarrior.simulation.levels import Level
from pythonwarrior.simulation.hero import GrowthTypes


class BodyArmorTestCase(TestCase):

    def setUp(self):
        self.body_armor = BodyArmor()

    def test_defend_against_spell_does_full_damage(self):
        self.assertEqual(self.body_armor.defend_against_spell(10), 10)

    def test_apply_step_effect_takes_hero_arg(self):
        with self.assertRaises(TypeError):
            self.body_armor.apply_step_effect()
        self.body_armor.apply_step_effect(Mock())


class HeroNameTestCase(TestCase):

    def setUp(self):
        self.name = HeroName("Kefka")

    def test_get_letter_value(self):
        self.assertEqual(self.name.get_letter_value("A"), HeroName.LETTER_VALUE_TABLE["A"])

    def test_get_name_sum_returns_0_for_empty_string(self):
        self.name.name = ""
        self.assertEqual(0, self.name.get_name_sum())

    def test_get_name_sum_returns_sum_of_first_four_letters(self):
        self.name.name = "hhhhAAAA"
        self.assertEqual(4, self.name.get_name_sum())

    def test_get_name_sum_returns_43_for_Erdrick(self):
        self.name.name = "Erdrick"
        self.assertEqual(43, self.name.get_name_sum())

    def test_get_name_sum_returns_52_for_Kefka(self):
        self.name.name = "Kefka"
        self.assertEqual(47, self.name.get_name_sum())

    def test_get_growth_type_for_Kefka_returns_STRENGTH_AND_AGILITY(self):
        self.assertEqual(GrowthTypes.STRENGTH_AND_AGILITY, self.name.get_growth_type())

    def test_get_growth_type_for_Erdrick_returns_STRENGTH_AND_AGILITY(self):
        self.name.name = "Erdrick"
        self.assertEqual(GrowthTypes.STRENGTH_AND_AGILITY, self.name.get_growth_type())

    def test_get_growth_type_for_g_returns_HP_AND_MP(self):
        self.name.name = "g"
        self.assertEqual(GrowthTypes.HP_AND_MP, self.name.get_growth_type())

    def test_get_growth_type_for_h_returns_HP_AND_MP(self):
        self.name.name = "h"
        self.assertEqual(GrowthTypes.STRENGTH_AND_HP, self.name.get_growth_type())

    def test_get_growth_type_for_i_returns_HP_AND_MP(self):
        self.name.name = "i"
        self.assertEqual(GrowthTypes.AGILITY_AND_MP, self.name.get_growth_type())

    def test_validate_name(self):
        self.assertTrue(self.name.validate())

    def test_invalid_name(self):
        self.name.name = "!@#$!^!#$&!"
        self.assertFalse(self.name.validate())


class HeroTestCase(TestCase):

    def setUp(self):
        self.inventory = Inventory()
        self.first_level = Level(1, 0, None)
        self.second_level = Level(2, 100, None)
        self.third_level = Level(3, 300, None)
        self.second_level.next_level = self.third_level
        self.first_level.next_level = self.second_level
        self.hero = Hero(HeroName("Kefka"), self.inventory, self.first_level)
        self.item = Mock(Item)
        self.item.name = "Foo"
        self.item.is_stackable = False
        self.hero.take_item(self.item)

    def test_hero_has_inventory(self):
        self.assertIs(self.hero.inventory, self.inventory)

    def test_hero_can_use_items(self):
        self.hero.take_item(self.item)
        self.hero.use_item("Foo")
        self.item.use.assert_called_once_with(self.hero)

    def test_hero_can_restore_mp_up_to_max_mp(self):
        with patch("tests.test_hero.Hero.max_mp") as mock_max_mp:
            mock_max_mp.__get__ = Mock(return_value=10)
            self.hero.restore_mp(50)
            self.assertEqual(self.hero.mp, 10)

    def test_hero_can_spend_mp(self):
        self.hero.mp = 10
        self.hero.spend_mp(5)
        self.assertEqual(self.hero.mp, 5)

    def test_hero_initializes_with_no_armor(self):
        self.assertIsInstance(self.hero.armor, NoBodyArmor)

    def test_hero_initializes_with_no_weapon(self):
        self.assertIsInstance(self.hero.weapon, NoWeapon)

    def test_hero_initializes_with_no_shield(self):
        self.assertIsInstance(self.hero.shield, NoArmor)

    def test_hero_initializes_with_no_ring(self):
        self.assertIsInstance(self.hero.ring, NoWeapon)

    def test_hero_initializes_with_no_accessory(self):
        self.assertIsInstance(self.hero.accessory, NoArmor)

    def test_when_hero_mp_is_less_than_spend_amount_InsufficientMPException_is_raised(self):
        with self.assertRaises(Hero.InsufficientMPException):
            self.hero.spend_mp(10)

    def test_when_defending_against_spell_damage_armor_can_effect_outcome(self):
        armor = Mock(BodyArmor)
        armor.defend_against_spell = lambda amount: amount // 2
        self.hero.equip_armor(armor)
        self.hero.hp = 20
        self.hero.defend_against_spell(20, 20)
        self.assertEqual(self.hero.hp, 10)

    def test_when_taking_a_step_armor_can_effect_player_stats(self):

        def step_effect(hero):
            hero.hp += 1

        armor = Mock(BodyArmor)
        armor.apply_step_effect = step_effect
        self.hero.equip_armor(armor)
        self.hero.step()
        self.assertEqual(self.hero.hp, 1)

    def test_attack_power_incorporates_weapon(self):
        weapon = Mock(Weapon)
        weapon.add_attack_power = lambda x: x + 10
        self.hero.wield(weapon)
        self.assertEqual(self.hero.attack_power, 10)

    def test_attack_power_incorporates_ring(self):
        fighters_ring = Mock(Armor)
        fighters_ring.add_attack_power = lambda x: x + 2
        self.hero.wear_ring(fighters_ring)
        self.assertEqual(self.hero.attack_power, 2)

    def test_defense_power_incorporates_armor(self):
        armor = Mock(BodyArmor)
        armor.add_defense_power = lambda x: x + 10
        self.hero.equip_armor(armor)
        self.assertEqual(self.hero.defense_power, 10)

    def test_defense_power_incorporates_shield(self):
        shield = Mock(Armor)
        shield.add_defense_power = lambda x: x + 10
        self.hero.equip_shield(shield)
        self.assertEqual(self.hero.defense_power, 10)

    def test_defense_power_incorporates_accessory(self):
        dragon_scale = Mock(Armor)
        dragon_scale.add_defense_power = lambda x: x + 2
        self.hero.wear_accessory(dragon_scale)
        self.assertEqual(self.hero.defense_power, 2)

    def test_can_gain_experience(self):
        self.assertEqual(self.hero.experience, 0)
        self.hero.gain_experience(100)
        self.assertEqual(self.hero.experience, 100)

    def test_required_experience_for_next_level_is_0_if_at_max(self):
        self.hero.level = self.third_level
        self.assertEquals(self.hero.get_experience_to_level(), 0)

    def test_can_calculate_required_experience_to_level(self):
        self.assertEquals(self.hero.get_experience_to_level(), self.hero.level.next_level.experience)

    def test_can_gain_levels(self):
        self.assertIs(self.hero.level, self.first_level)
        self.hero.gain_experience(self.second_level.experience)
        self.assertIs(self.hero.level, self.second_level)

    def test_can_gain_multiple_levels_at_once(self):
        self.assertIs(self.hero.level, self.first_level)
        self.hero.gain_experience(self.third_level.experience)
        self.assertIs(self.hero.level, self.third_level)

    def test_max_hp_delegates_to_level(self):
        self.hero.level = Mock(Level)
        self.hero.level.get_adjusted_hp.return_value = "foo"
        self.assertEqual(self.hero.max_hp, "foo")
        self.hero.level.get_adjusted_hp.assert_called_once_with(self.hero.name)
        
    def test_max_mp_delegates_to_level(self):
        self.hero.level = Mock(Level)
        self.hero.level.get_adjusted_mp.return_value = "foo"
        self.assertEqual(self.hero.max_mp, "foo")
        self.hero.level.get_adjusted_mp.assert_called_once_with(self.hero.name)