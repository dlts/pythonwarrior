from unittest import TestCase

from mock import Mock

from pythonwarrior.simulation.battle import Battle, BattleStates
from pythonwarrior.simulation.entity import Entity


class BattleTestCase(TestCase):

    def setUp(self):
        self.entity_a = Mock(Entity())
        self.entity_b = Mock(Entity())
        self.entity_a.preemptively_flee.return_value = False
        self.entity_b.preemptively_flee.return_value = False
        self.entity_a.is_dead.return_value = False
        self.entity_b.is_dead.return_value = False
        self.battle = Battle(self.entity_a, self.entity_b)

    def test_should_start_in_PRE_BATTLE_state(self):
        self.assertEqual(self.battle.state, BattleStates.PRE_BATTLE)

    def test_first_update_should_decide_initiative_and_set_state_to_GET_ACTION(self):
        self.battle.update()
        acting = self.battle.get_acting_entity()
        self.assertIs(acting, self.entity_a)
        self.assertEqual(self.battle.state, BattleStates.GET_ACTION)

    def test_first_update_should_give_both_combatants_a_chance_to_preemptively_flee(self):
        self.battle.update()
        self.battle.entity_a.preemptively_flee.assert_called_once_with(self.entity_b)
        self.battle.entity_b.preemptively_flee.assert_called_once_with(self.entity_a)

    def test_when_either_entity_preemptively_flees_the_battle_is_over(self):
        self.entity_a.preemptively_flee.return_value = True
        self.battle.update()
        self.assertTrue(self.battle.is_over())

    def test_when_state_is_GET_ACTION_and_action_name_and_target_has_been_set_should_transition_to_EXECUTE_ACTION(self):
        self.battle.update()
        self.assertEqual(self.battle.state, BattleStates.GET_ACTION)
        self.battle.update()
        self.assertEqual(self.battle.state, BattleStates.GET_ACTION)
        self.battle.set_action_name("fight")
        self.battle.set_target(self.entity_a)
        self.battle.update()
        self.assertEqual(self.battle.state, BattleStates.EXECUTE_ACTION)

    def test_target_may_only_bet_set_to_entity_a_or_entity_b(self):
        self.battle.set_target(self.entity_a)
        self.battle.set_target(self.entity_b)
        with self.assertRaises(Battle.InvalidTargetException):
            self.battle.set_target(Entity())

    def test_when_state_is_EXECUTE_ACTION_acting_entity_act_is_invoked_with_target(self):
        self.battle.acting_entity = self.entity_a
        self.battle.state = BattleStates.EXECUTE_ACTION
        self.battle.set_action_name('fight')
        self.battle.set_target(self.entity_b)
        self.battle.update()
        self.entity_a.act.assert_called_once_with('fight', self.entity_b)

    def test_when_entity_a_doesnt_win_initiative_entity_b_should_be_acting_entity(self):
        self.entity_a.wins_initiative.return_value = False
        self.battle.decide_initiative()
        self.assertEqual(self.battle.acting_entity, self.entity_b)

    def test_swap_acting_entity(self):
        self.battle.acting_entity = self.entity_a
        self.battle.swap_acting_entity()
        self.assertIs(self.battle.acting_entity, self.entity_b)
        self.battle.swap_acting_entity()
        self.assertIs(self.battle.acting_entity, self.entity_a)

    def test_when_state_is_EXECUTE_ACTION_and_either_entity_dies_transition_to_POST_BATTLE(self):
        self.battle.acting_entity = self.entity_a
        self.battle.state = BattleStates.EXECUTE_ACTION
        self.battle.set_action_name('fight')
        self.battle.set_target(self.entity_b)
        self.entity_b.is_dead.return_value = True
        self.battle.update()
        self.assertEqual(self.battle.state, BattleStates.POST_BATTLE)

    def test_when_state_is_EXECUTE_ACTION_and_neither_entity_dies_acting_entity_is_swapped(self):
        self.battle.acting_entity = self.entity_a
        self.battle.state = BattleStates.EXECUTE_ACTION
        self.battle.on_execute_action()
        self.assertIs(self.battle.acting_entity, self.entity_b)

    def test_when_state_is_EXECUTE_ACTION_and_neither_entity_dies_transition_to_GET_ACTION(self):
        self.battle.acting_entity = self.entity_a
        self.battle.state = BattleStates.EXECUTE_ACTION
        self.battle.on_execute_action()
        self.assertEqual(self.battle.state, BattleStates.GET_ACTION)

    def test_when_state_is_EXECUTE_ACTION_and_either_player_dies_the_loser_rewards_the_victor(self):
        self.battle.acting_entity = Mock(Entity)
        self.entity_b.is_dead.return_value = True
        self.battle.on_execute_action()
        self.entity_b.reward_entity.assert_called_once_with(self.entity_a)

        self.battle.state = BattleStates.EXECUTE_ACTION
        self.entity_b.is_dead.return_value = False
        self.entity_a.is_dead.return_value = True
        self.battle.on_execute_action()
        self.entity_a.reward_entity.assert_called_once_with(self.entity_b)

    def test_when_state_is_POST_BATTLE_update_is_non_operation(self):
        self.battle.state = BattleStates.POST_BATTLE
        self.battle.update()
        self.assertEqual(self.battle.state, BattleStates.POST_BATTLE)
