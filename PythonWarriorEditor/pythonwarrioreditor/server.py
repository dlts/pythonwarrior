from flask import Flask, send_from_directory, jsonify, request

from pythonwarrior.databases.interface import EffectRecord

app = Flask(__name__, static_folder="app", static_url_path="/")


def dynamic_import(name):
    mod_name = '.'.join(name.split('.')[0:-1])
    class_name = name.split('.')[-1]
    mod = __import__(mod_name)
    module_object = mod
    for part in mod_name.split('.')[1:]:
        module_object = getattr(module_object, part)
    return getattr(module_object, class_name)


def import_mapper_factory():
    MapperClass = dynamic_import(app.config.get('DB_IMPLEMENTATION'))
    return MapperClass(**app.config.get('DB_CONF'))


mapper_factory = None


def get_effect_mapper():
    global mapper_factory
    if mapper_factory is None:
        mapper_factory = import_mapper_factory()
    return mapper_factory.create_mapper(EffectRecord)


@app.route("/")
def index():
    return send_from_directory("app", "index.html")


@app.route("/tests")
def test_index():
    return send_from_directory("app", "test-index.html")


@app.route('/<path:path>')
def static_proxy(path):
    return app.send_static_file(path)


@app.route("/effects", methods=["GET"])
def get_effects():
    mapper = get_effect_mapper()
    return jsonify(collection=[r._asdict() for r in mapper.read_all()])


@app.route("/effects/<effect_name>", methods=["DELETE"])
def delete_effect(effect_name):
    print("TODO: implement delete_effect")
    mapper = get_effect_mapper()
    mapper.delete(effect_name)
    return ""


@app.route("/effects/<effect_name>", methods=["POST"])
def post_effect(effect_name):
    print("TODO: implement post_effect")
    print(request.get_json())
    return request.get_json()


@app.route("/effects/<effect_name>", methods=["PUT"])
def put_effect(effect_name):
    print("TODO: implement put_effect")
    data = request.get_json()
    record = EffectRecord(data["name"], data["code"])
    mapper = get_effect_mapper()
    mapper.create(record)
    return jsonify(data)
