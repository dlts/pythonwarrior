/*global require*/

require.config({
  urlArgs: "bust=" +  (new Date()).getTime(),
  paths: {
    'backbone': 'lib/backbone',
    'underscore': 'lib/underscore',
    'jquery': 'lib/jquery',
    'bootstrap': 'lib/bootstrap',
    'text': 'lib/text'
  },
  shim: {
    'backbone': {
      deps: ['underscore', 'jquery'],
      exports: 'Backbone'
    },
    'underscore': {
      exports: '_',
      init: function (_) {
        return this._.noConflict();
      }
    },
    'jquery': {
      exports: '$',
      init: function (jQuery) {
        return this.$.noConflict();
      }
    },
    'bootstrap': {
      deps: ['jquery']
    }
  }});


require(['backbone', 'jquery', 'router', 'viewport', 'bootstrap'], function(Backbone, $, router, ViewPort) {
  $(function () {
    var viewport = new ViewPort($('#viewport'));
    
    Backbone.history.start({
      pushState: false
    });

    router.on('route', viewport.handleRoute);

    if (window.location.hash.length > 0) {
      Backbone.history.loadUrl(Backbone.history.fragment);
    } else {
      viewport.handleRoute('dashboard');
    }
  });    
});
