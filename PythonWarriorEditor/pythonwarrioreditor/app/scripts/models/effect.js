/*global define*/
define(['backbone'], function (Backbone) {
  var effect = {};

  effect.Effect = Backbone.Model.extend({
    idAttribute: 'name',
    validate: function(attrs, options) {
      if (typeof attrs.name === 'undefined' ||
          attrs.name.length === 0) {
        return 'Effect name is required';
      }
      if (typeof attrs.code === 'undefined' ||
          attrs.code.length === 0) {
        return 'Effect code is required';
      }
    }
  });

  effect.EffectCollection = Backbone.Collection.extend({
    url: 'effects',
    comparator: 'name',  
    model: effect.Effect,
    parse: function(response){
      return response.collection;
    }    
  });
  
  return effect;
});
