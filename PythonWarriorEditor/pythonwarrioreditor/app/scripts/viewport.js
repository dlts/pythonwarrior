/*global define require*/
define(function () {
  var ViewPort = function (viewportElement) {
    var self = this;

    this.currentForm = null;
    this.viewportElement = viewportElement;
    
    this.handleRoute = function (route) {
      if (typeof route === 'undefined' || route === null) {
        throw new Error('Invalid route');
      }
      self.viewportElement.html('');
      require(['forms/' + route + '/' + route], self.loadForm);
    };

    this.loadForm = function (formModule) {
      if (self.currentForm !== null) {
        self.currentForm.tearDown();
      }

      self.currentForm = formModule.createForm();
      self.currentForm.setElement(self.viewportElement);
      self.currentForm.render();
    };

  };
  
  return ViewPort;
});
