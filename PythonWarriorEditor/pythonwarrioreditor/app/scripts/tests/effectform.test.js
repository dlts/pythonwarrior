/*global define module test ok fail throws equal require expect test*/
define(['forms/effectform/effectform', 'models/effect', 'jquery', 'sinon'], function (effectform, effect, $, sinon) {
  module('effectform.EffectForm', {
    beforeEach: function () {
      this.sandbox = sinon.sandbox.create();
      this.tmpl = '<div><select name="effects"></select>' +
        '<input type="text" name="name"><textarea name="code"></textarea>' +
        '<button class="save-button"></button>' +
        '<button class="delete-button"></button></div>';
      this.elem = $('<div></div>');
      
      this.form = effectform.createForm({});
      this.form.setElement(this.elem);
    },

    afterEach: function () {
      this.sandbox.restore();
    }
  });

  test('can create the form', function () {
    ok(typeof this.form !== 'undefined');
  });

  var renderForm = function () {
    var self = this;

    try {
      this.sandbox.stub(this.form.collection, 'fetch', function () {
        var deferred = new $.Deferred();
        deferred.resolve();
        return deferred.promise();
      });
    } catch (e) {
      if (!(e instanceof TypeError)) {
        throw e;
      }
    };

    try {
      this.sandbox.stub(window, 'require', function (modules, cb) {
        cb(self.tmpl);
      });
    } catch (e) {
      if (!(e instanceof TypeError)) {
        throw e;
      }
    };

    this.form.render();
  };

  test('can render its template to the form', function () {
    renderForm.call(this);
    equal(this.elem.html(), this.tmpl);
  });

  test('before the first render EffectForm fetches the effects collection', function () {
    var stub = this.sandbox.stub(this.form.collection, 'fetch', function () {
      var deferred = new $.Deferred();
      deferred.resolve();
      return deferred.promise();
    });

    renderForm.call(this);
    renderForm.call(this);

    ok(stub.calledOnce);
  });

  test('selectedRecordChanged binds the selected effect data to the viewModel', function () {
    this.form.getSelectedModel = function () {
      return new effect.Effect({name: 'foobar', code: 'foobar-code'});
    };
    this.form.selectedRecordChanged();
    equal(this.form.viewModel.get('name'), 'foobar');
    equal(this.form.viewModel.get('code'), 'foobar-code');
  });
  
});
