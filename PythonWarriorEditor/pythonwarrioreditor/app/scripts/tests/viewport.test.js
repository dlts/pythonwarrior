/*global define module test ok fail throws equal require expect*/
define(['viewport', 'jquery', 'sinon'], function (ViewPort, $, sinon) {
  module('ViewPort');
    
  var assertHandleRouteError = function (route) {
    var vp = new ViewPort();
    throws(function () {
      vp.handleRoute(route);
    }, /invalid route/i);
  };

  var createMockForm = function () {
    return {
      setElement: function () {},
      render: function () {}
    };
  };
  
  test('When ViewPort.handleRoute is passed a NULL an Error is thrown', function () {
    assertHandleRouteError(null);
  });

  test('When ViewPort.handleRoute is passed undefined an Error is thrown', function () {
    assertHandleRouteError();    
  });
    

  module('ViewPort.handleRoute', {
    beforeEach: function () {
      this.elem = $('<div></div>');
      this.vp = new ViewPort(this.elem);
      this.sandbox = sinon.sandbox.create();
    },
    afterEach: function () {
      this.sandbox.restore();
    }
  });

  test('When ViewPort.handleRoute changes the viewport it clears the viewport HTML first', function () {
    this.sandbox.stub(window, 'require');
    this.elem.text('clear me');
    this.vp.handleRoute('test');
    equal(this.elem.text(), '');
  });

  test('When ViewPort.handleRoute changes the viewport it requests a form module', function () {
    expect(0);
    var mock = this.sandbox.mock(window), formName = 'some-form';
    mock.expects('require').withExactArgs(['forms/' + formName + '/' + formName], this.vp.loadForm);
    this.vp.handleRoute(formName);
    mock.verify();
    mock.restore();
  });

  test('When ViewPort.handleRoute changes the viewport, it creates a new form', function () {
    var called = false, self = this;
    var stub = this.sandbox.stub(window, 'require', function (module, cb) {
      var formModule = {createForm: function () {}}, mock;
      mock = self.sandbox.mock(formModule);
      mock.expects('createForm').once().returns(createMockForm());
      cb(formModule);
      mock.verify();
      called = true;      
    });

    this.vp.handleRoute('form-name');
    ok(called);
  });

  test('Given that a previous form was loaded, the previous form is torn down before loading the new one', function () {
    var called = false, self = this;
    var stub = this.sandbox.stub(window, 'require', function (module, cb) {
      var formModule = {createForm: function () {
        var mockForm = createMockForm();
        mockForm.tearDown = function () {
          called = true;
        };
        return mockForm;
      }};
      cb(formModule);
    });
    this.vp.handleRoute('form-name');
    this.vp.handleRoute('form-name');    
    ok(called);
  });

  test('When ViewPort.handleRoute changes the viewport, the new form is passed the viewport element', function () {
    var called = false, self = this;
    var stub = this.sandbox.stub(window, 'require', function (module, cb) {
      var formModule = {createForm: function () {
        var mockForm = createMockForm();
        mockForm.setElement = function (viewportElement) {
            equal(viewportElement, self.elem);
            called = true;
        };
        return mockForm;
      }};
      cb(formModule);
    });
    this.vp.handleRoute('form-name');

    ok(called);
  });

  test('When ViewPort.handleRoute changes the viewport, the new form is rendered', function () {
    var called = false, self = this;
    var stub = this.sandbox.stub(window, 'require', function (module, cb) {
      var formModule = {createForm: function () {
        var mockForm = createMockForm();
        mockForm.render = function () {
          called = true;
        };
        return mockForm;
      }};
      cb(formModule);
    });
    this.vp.handleRoute('form-name');

    ok(called);
  });
  
  
});
