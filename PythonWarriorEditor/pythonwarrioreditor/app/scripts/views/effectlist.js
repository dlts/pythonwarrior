/*global define*/
define(['backbone', 'text!views/effectlist.tpl.html'], function (Backbone, effectListTemplate) {
  var effectList = {};

  effectList.EffectListModel = Backbone.Model.extend({
    defaults: {
      "selectedRecordId": null
    }
  });

  effectList.EffectList = Backbone.View.extend({

    setViewModel: function (viewModel) {
      this.viewModel = viewModel;
    },

    initialize: function () {
      this.viewModel = null;
      this.listenTo(this.collection, 'add', this.render);
      this.listenTo(this.collection, 'remove', this.render);
      this.listenTo(this.collection, 'destroy', this.render);
      this.listenTo(this.collection, 'sync', this.render);
    },

    template: _.template(effectListTemplate),
    events: {
      'change select': 'onEffectSelected'
    },

    render: function () {
      this.$el.html(this.template());
      var select = this.$el.find('select');
      this.collection.forEach(function (effect) {
        select.append('<option>' + effect.get('name') + '</option>');
      });
      return this;
    },

    onEffectSelected: function () {
      var selectedName = this.$el.find('select').val();
      if (this.viewModel) {
        this.viewModel.set('selectedRecordId', selectedName);
      }
    }
  });

  return effectList;
});
