/*global define*/
define(["backbone"], function (Backbone) {
  var Router = Backbone.Router.extend({
    routes: {
      'effects': 'effectform',
      '*page': 'dashboard'
    }
  });
  return new Router();
});
