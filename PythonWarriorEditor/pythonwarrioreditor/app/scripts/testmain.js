/*global require*/

require.config({
  urlArgs: "bust=" +  (new Date()).getTime(),
  paths: {
    'backbone': 'lib/backbone',
    'underscore': 'lib/underscore',
    'jquery': 'lib/jquery',
    'bootstrap': 'lib/bootstrap',
    'text': 'lib/text',
    'qunit': 'lib/qunit',
    'sinon': 'lib/sinon'
  },
  shim: {
    'backbone': {
      deps: ['underscore', 'jquery'],
      exports: 'Backbone'
    },
    'underscore': {
      exports: '_',
      init: function (_) {
        return this._.noConflict();
      }
    },
    'jquery': {
      exports: '$',
      init: function (jQuery) {
        return this.$.noConflict();
      }
    },
    'bootstrap': {
      deps: ['jquery']
    },
    'qunit': {
      deps: ['jquery'],
      exports: 'QUnit'
    }
  }});


require(['qunit'], function (QUnit) {
  QUnit.config.autostart = false;

  var deps = ['backbone', 'jquery'],
      push = function (test) {
        deps.push('tests/' + test + '.test');
      };

  push('viewport');
  push('effectform');
  
  require(deps, function() {
    QUnit.start();
    QUnit.load();
  });
  
});
