/*global define, require*/
define(['backbone'], function (Backbone) {
  var dashboard = {};

  dashboard.Dashboard = Backbone.View.extend({
    render: function () {
      var self = this;
      require(['text!forms/dashboard/dashboard.tpl.html'], function (template) {
        self.$el.html(template);
      });
    },

    tearDown: function () {}
  });

  dashboard.createForm = function () {
    return new dashboard.Dashboard();
  };
  
  return dashboard;
});
