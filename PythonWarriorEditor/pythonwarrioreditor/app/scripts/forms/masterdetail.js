define(['backbone'], function (Backbone) {
  var masterDetail = {};

  masterDetail.MasterDetailForm = Backbone.View.extend({

    events: {
      'change input'          : 'inputChanged',
      'change textarea'       : 'inputChanged',
      'change [name=records]' : 'selectedRecordChanged'
    },

    initialize: function (args) {
      this.viewModel = args.viewModel;
      this.listenTo(this.viewModel, 'change', this.syncFormWithModel);
    },

    syncFormWithModel: function (model) {
      for (var attributeName in model.attributes) {
        if (model.hasChanged(attributeName)) {
          this.updateAttributeByName(attributeName);
        }
      }
    },

    updateAttributeByName: function (attributeName) {
      this.$el.find('[name=' + attributeName + ']').val(this.viewModel.get(attributeName));
    },

    inputChanged: function (event) {
      var el = $(event.target);
      this.viewModel.set(el.attr('name'), el.val());
    },

    selectedRecordChanged: function () {
      var model = this.getSelectedModel();
      this.viewModel.set('name', model.get('name'));
      this.viewModel.set('code', model.get('code'));
    },

    getSelectedModel: function () {
      var name = this.getListElement().val(),
          model = this.collection.get(name);
      return model;
    },

    getListElement: function () {
      return this.$el.find('[name=records]');
    },

    bindCollection: function () {
      var el = this.getListElement();
      el.html('');
      this.collection.forEach(function (effect) {
        var name = effect.get('name');
        el.append('<option value="' + name + '">' + name + '</option>');
      });
    },

    tearDown: function () {}
  });

  return masterDetail;
});