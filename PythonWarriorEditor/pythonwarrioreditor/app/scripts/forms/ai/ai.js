/*global define*/
define(["backbone", "text!forms/ai/ai.tpl.html", "models/effect", "views/effectlist"], function (Backbone, aiTemplate, effect, effectList) {
  var ai = {};

  ai.setup = function (viewport) {
    viewport.html(aiTemplate);

    $('#selected-effect-code').on('keydown', function (event) {
      if (event.keyCode === 9) {
        $('#selected-effect-code').val($('#selected-effect-code').val() + '    ');
        event.preventDefault();        
      } else if (event.keyCode === 13) {
        $('#selected-effect-code').val($('#selected-effect-code').val() + '\n');
        event.preventDefault();
      }
    });
    
    var effects = new effect.Effects(),
        listModel = new effectList.EffectListModel({}),
        list = new effectList.EffectList({
          collection: effects
        });
    list.setViewModel(listModel);

    listModel.on('change:selectedRecordId', function (model, newValue) {
      var selectedEffect = effects.get(newValue);
      $('#selected-effect-name').val(selectedEffect.get('name'));
      $('#selected-effect-code').val(selectedEffect.get('code'));                                      
    });
    
    $('#effect-list').append(list.render().$el);
    effects.fetch();
  };

  ai.teardown = function () {};
  
  return ai;
});
