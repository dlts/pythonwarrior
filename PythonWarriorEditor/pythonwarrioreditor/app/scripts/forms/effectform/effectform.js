/*global define require*/
define(['forms/masterdetail', 'models/effect', 'backbone'], function (masterDetail, effect, Backbone) {
  var effectForm = {};

  effectForm.createForm = function () {
    var collection = new effect.EffectCollection(),
        viewModel = new effectForm.EffectFormViewModel();
    collection.fetch();
    return new effectForm.EffectForm({collection: collection,
                                      viewModel: viewModel});
  };

  effectForm.EffectFormViewModel = Backbone.Model.extend({
    defaults: {
      selectedRecordId: null,
      name: '',
      code: ''
    }
  });

  effectForm.EffectForm = masterDetail.MasterDetailForm.extend({
    events: {
      'click .save-button'    : 'saveEffect',
      'click .delete-button'  : 'deleteEffect',
      'keydown textarea'      : 'permitTabs'
    },

    initialize: function (args) {
      _.extend(this.events, effectForm.EffectForm.__super__.events);
      effectForm.EffectForm.__super__.initialize.call(this, args);
      this.hasEverBeenRendered = false;
    },

    render: function () {
      var self = this;
      require(['text!forms/effectform/effectform.tpl.html'], function (template) {
        if (!self.hasEverBeenRendered) {
          self.collection.fetch().done(function () {
            self.$el.html(template);
            self.bindCollection();
            self.hasEverBeenRendered = true;
          });
        } else {
          self.$el.html(template);
        }
      });
      return this;
    },

    saveEffect: function () {
      var model = this.getSelectedModel(), newModel,
          name = this.viewModel.get('name'),
          code = this.viewModel.get('code');

      if (typeof model === 'undefined' || model.get('name') !== name) {
        newModel = new effect.Effect({'name': name, 'code': code});
        this.collection.add(newModel);
        newModel.save();
      } else {
        model.set('name', name);
        model.set('code', code);
        model.save();
      }

      this.bindCollection();
    },

    deleteEffect: function () {
      var model = this.getSelectedModel();
      model.destroy();
      this.bindCollection();
    },

    permitTabs: function (event) {
      if (event.keyCode === 9 && !event.shiftKey) {
        $(event.target).val($(event.target).val() + '    ');
        event.preventDefault();
      }
    }
  });

  return effectForm;
});
