/*global define*/
define(["text!forms/levels/levels.tpl.html"], function (levelsTemplate) {
  var levels = {};

  levels.setup = function (viewport) {
    viewport.html(levelsTemplate);
  };

  levels.teardown = function () {};
  
  return levels;
});
