from setuptools import setup

setup(
    name='PythonWarrior Database Editor Application',
    version='',
    packages=["pythonwarrioreditor"],
    namespaces=["pythonwarrioreditor"],
    url='',
    license='',
    author='wkmanire',
    author_email='williamkmanire@gmail.com',
    description='',
)
