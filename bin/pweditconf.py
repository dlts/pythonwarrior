from os.path import join
DEBUG = True
HOST = "0.0.0.0"
#DB_IMPLEMENTATION = "pythonwarrior.databases.memory.MemoryMapperFactory"
DB_IMPLEMENTATION = "pythonwarrior.databases.sqlite.SQLiteMapperFactory"
DB_CONF = dict()
STATIC_FOLDER = join("/", "home", "wkmanire", "Persona", "pythonwarrior",
                     "PythonWarriorEditor", "pythonwarrioreditor", "app")
