#!/usr/bin/env python

from pythonwarrioreditor.server import app

if __name__ == "__main__":
    app.config.from_object('pythonwarrioreditor.defaultsettings')
    app.config.from_envvar('PYTHONWARRIOREDITOR_SETTINGS')
    app.run(debug=app.config.get('DEBUG', False),
            host=app.config.get('HOST', '127.0.0.1'))
